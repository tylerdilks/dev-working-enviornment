
<!DOCTYPE html>
<html class="no-js" dir="ltr" lang="en-US" xmlns:fb="http://ogp.me/ns/fb#" xmlns:addthis="http://www.addthis.com/help/api-spec"  prefix="og: http://ogp.me/ns#"><!--<![endif]-->
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="keywords" content="" />
		<meta charset="UTF-8" />
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel='stylesheet' id='styles-child-css'  href='https://www.fastcapital360.com/wp-content/themes/porto-child/style.css?ver=4.8' type='text/css' media='all' />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
		<link rel='stylesheet' id='porto-style-css'  href='https://www.fastcapital360.com/wp-content/themes/porto/style.css?ver=4.8' type='text/css' media='all' />
<script
  src="https://code.jquery.com/jquery-1.10.2.min.js"
  integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg="
  crossorigin="anonymous"></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js'></script>
<script
  src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"
  integrity="sha256-FgiaQnQazF/QCrF9qSvpRY6PACn9ZF8VnlgqfqD1LsE="
  crossorigin="anonymous"></script>
<script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i');

		body .ah-form-container label {
    opacity: 1;
    position: relative;
    bottom: 9px;
    background: #2b3556;
   /* box-shadow: 1px 2px 2px 0px rgba(0,0,0,0.20); */
    border-radius: 6px;
    font-size: 21px;
    font-weight: 400;
    /* opacity: 0.8; */
    width: 580px;
    min-height: 54px;
    padding-left: 54px;
    padding-top: 3px;
    text-align: left;
    margin: 8px 0;
    color: #fff;
    cursor: pointer;
	}
	.hvr-grow {
    display: inline-block;
    vertical-align: middle;
    transform: translateZ(0);
    box-shadow: 0 0 1px rgba(0, 0, 0, 0);
    backface-visibility: hidden;
    -moz-osx-font-smoothing: grayscale;
    transition-duration: 0.3s;
    transition-property: transform;
	}
	body form#sliderForm {
    clear: both;
    padding-top: 25px;
    max-width: 1270px;
    margin: 0 auto;
	}
	body input[type="radio"] {
    display: none;
	}
	.bx-controls {
    display: none;
	}
	body.single-landing-page input[type="radio"] + label span {
    display: inline-block;
    width: 30px;
    height: 32px;
    margin-right: 35px;
    background: url(checkbox-sprite.png);
    position: relative;
    top: 6px;
	}
	.hvr-grow:hover, .hvr-grow:focus, .hvr-grow:active {
    transform: scale(1.1);
		background: #8AC657!important;
	}
	
	
/** RESET AND LAYOUT
===================================*/

.bx-wrapper {
	position: relative;
	margin: 0 auto 60px;
	padding: 0;
	*zoom: 1;
}

.bx-wrapper img {
	max-width: 100%;
	display: block;
}

/** THEME
===================================*/

.bx-wrapper .bx-viewport {
	left: -5px;
	background: #fff;
	
	/*fix other elements on the page moving (on Chrome)*/
	-webkit-transform: translatez(0);
	-moz-transform: translatez(0);
    	-ms-transform: translatez(0);
    	-o-transform: translatez(0);
    	transform: translatez(0);
}

.bx-wrapper .bx-pager,
.bx-wrapper .bx-controls-auto {
	position: absolute;
	bottom: -30px;
	width: 100%;
}

/* LOADER */

.bx-wrapper .bx-loading {
	min-height: 50px;
	height: 100%;
	width: 100%;
	position: absolute;
	top: 0;
	left: 0;
	z-index: 2000;
}

/* PAGER */

.bx-wrapper .bx-pager {
	text-align: center;
	font-size: .85em;
	font-family: Arial;
	font-weight: bold;
	color: #666;
	padding-top: 20px;
}

.bx-wrapper .bx-pager .bx-pager-item,
.bx-wrapper .bx-controls-auto .bx-controls-auto-item {
	display: inline-block;
	*zoom: 1;
	*display: inline;
}

.bx-wrapper .bx-pager.bx-default-pager a {
	background: #666;
	text-indent: -9999px;
	display: block;
	width: 10px;
	height: 10px;
	margin: 0 5px;
	outline: 0;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}

.bx-wrapper .bx-pager.bx-default-pager a:hover,
.bx-wrapper .bx-pager.bx-default-pager a.active {
	background: #000;
}

/* DIRECTION CONTROLS (NEXT / PREV) */

.bx-wrapper .bx-prev {
	left: 10px;
	background: url(images/controls.png) no-repeat 0 -32px;
}

.bx-wrapper .bx-next {
	right: 10px;
	background: url(images/controls.png) no-repeat -43px -32px;
}

.bx-wrapper .bx-prev:hover {
	background-position: 0 0;
}

.bx-wrapper .bx-next:hover {
	background-position: -43px 0;
}

.bx-wrapper .bx-controls-direction a {
	position: absolute;
	top: 50%;
	margin-top: -16px;
	outline: 0;
	width: 32px;
	height: 32px;
	text-indent: -9999px;
	z-index: 9999;
}

.bx-wrapper .bx-controls-direction a.disabled {
	display: none;
}

/* AUTO CONTROLS (START / STOP) */

.bx-wrapper .bx-controls-auto {
	text-align: center;
}

.bx-wrapper .bx-controls-auto .bx-start {
	display: block;
	text-indent: -9999px;
	width: 10px;
	height: 11px;
	outline: 0;
	background: url(images/controls.png) -86px -11px no-repeat;
	margin: 0 3px;
}

.bx-wrapper .bx-controls-auto .bx-start:hover,
.bx-wrapper .bx-controls-auto .bx-start.active {
	background-position: -86px 0;
}

.bx-wrapper .bx-controls-auto .bx-stop {
	display: block;
	text-indent: -9999px;
	width: 9px;
	height: 11px;
	outline: 0;
	background: url(images/controls.png) -86px -44px no-repeat;
	margin: 0 3px;
}

.bx-wrapper .bx-controls-auto .bx-stop:hover,
.bx-wrapper .bx-controls-auto .bx-stop.active {
	background-position: -86px -33px;
}

/* PAGER WITH AUTO-CONTROLS HYBRID LAYOUT */

.bx-wrapper .bx-controls.bx-has-controls-auto.bx-has-pager .bx-pager {
	text-align: left;
	width: 80%;
}

.bx-wrapper .bx-controls.bx-has-controls-auto.bx-has-pager .bx-controls-auto {
	right: 0;
	width: 35px;
}

/* IMAGE CAPTIONS */

.bx-wrapper .bx-caption {
	position: absolute;
	bottom: 0;
	left: 0;
	background: #666\9;
	background: rgba(80, 80, 80, 0.75);
	width: 100%;
}

.bx-wrapper .bx-caption span {
	color: #fff;
	font-family: Arial;
	display: block;
	font-size: .85em;
	padding: 10px;
}
	
		</style>
<script type="text/javascript">
		function nextSlide(){
    $('.bx-next').trigger('click');
    barWidth = barWidth + 15;
    progressBar.style.width = barWidth+ '%';
    $("html, body").animate({
     scrollTop:0
     },"slow");
}
function lastSlide(){
    $('.bx-prev').trigger('click');  
    barWidth = barWidth - 15;
    progressBar.style.width = barWidth+ '%';
}
	/**
 * BxSlider v4.1.2 - Fully loaded, responsive content slider
 * http://bxslider.com
 *
 * Copyright 2014, Steven Wanderski - http://stevenwanderski.com - http://bxcreative.com
 * Written while drinking Belgian ales and listening to jazz
 *
 * Released under the MIT license - http://opensource.org/licenses/MIT
 */
!function(t){var e={},s={mode:"horizontal",slideSelector:"",infiniteLoop:!0,hideControlOnEnd:!1,speed:500,easing:null,slideMargin:0,startSlide:0,randomStart:!1,captions:!1,ticker:!1,tickerHover:!1,adaptiveHeight:!1,adaptiveHeightSpeed:500,video:!1,useCSS:!0,preloadImages:"visible",responsive:!0,slideZIndex:50,touchEnabled:!0,swipeThreshold:50,oneToOneTouch:!0,preventDefaultSwipeX:!0,preventDefaultSwipeY:!1,pager:!0,pagerType:"full",pagerShortSeparator:" / ",pagerSelector:null,buildPager:null,pagerCustom:null,controls:!0,nextText:"Next",prevText:"Prev",nextSelector:null,prevSelector:null,autoControls:!1,startText:"Start",stopText:"Stop",autoControlsCombine:!1,autoControlsSelector:null,auto:!1,pause:4e3,autoStart:!0,autoDirection:"next",autoHover:!1,autoDelay:0,minSlides:1,maxSlides:1,moveSlides:0,slideWidth:0,onSliderLoad:function(){},onSlideBefore:function(){},onSlideAfter:function(){},onSlideNext:function(){},onSlidePrev:function(){},onSliderResize:function(){}};t.fn.bxSlider=function(n){if(0==this.length)return this;if(this.length>1)return this.each(function(){t(this).bxSlider(n)}),this;var o={},r=this;e.el=this;var a=t(window).width(),l=t(window).height(),d=function(){o.settings=t.extend({},s,n),o.settings.slideWidth=parseInt(o.settings.slideWidth),o.children=r.children(o.settings.slideSelector),o.children.length<o.settings.minSlides&&(o.settings.minSlides=o.children.length),o.children.length<o.settings.maxSlides&&(o.settings.maxSlides=o.children.length),o.settings.randomStart&&(o.settings.startSlide=Math.floor(Math.random()*o.children.length)),o.active={index:o.settings.startSlide},o.carousel=o.settings.minSlides>1||o.settings.maxSlides>1,o.carousel&&(o.settings.preloadImages="all"),o.minThreshold=o.settings.minSlides*o.settings.slideWidth+(o.settings.minSlides-1)*o.settings.slideMargin,o.maxThreshold=o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin,o.working=!1,o.controls={},o.interval=null,o.animProp="vertical"==o.settings.mode?"top":"left",o.usingCSS=o.settings.useCSS&&"fade"!=o.settings.mode&&function(){var t=document.createElement("div"),e=["WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var i in e)if(void 0!==t.style[e[i]])return o.cssPrefix=e[i].replace("Perspective","").toLowerCase(),o.animProp="-"+o.cssPrefix+"-transform",!0;return!1}(),"vertical"==o.settings.mode&&(o.settings.maxSlides=o.settings.minSlides),r.data("origStyle",r.attr("style")),r.children(o.settings.slideSelector).each(function(){t(this).data("origStyle",t(this).attr("style"))}),c()},c=function(){r.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>'),o.viewport=r.parent(),o.loader=t('<div class="bx-loading" />'),o.viewport.prepend(o.loader),r.css({width:"horizontal"==o.settings.mode?100*o.children.length+215+"%":"auto",position:"relative"}),o.usingCSS&&o.settings.easing?r.css("-"+o.cssPrefix+"-transition-timing-function",o.settings.easing):o.settings.easing||(o.settings.easing="swing"),f(),o.viewport.css({width:"100%",overflow:"hidden",position:"relative"}),o.viewport.parent().css({maxWidth:p()}),o.settings.pager||o.viewport.parent().css({margin:"0 auto 0px"}),o.children.css({"float":"horizontal"==o.settings.mode?"left":"none",listStyle:"none",position:"static"}),o.children.css("width",u()),"horizontal"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginRight",o.settings.slideMargin),"vertical"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginBottom",o.settings.slideMargin),"fade"==o.settings.mode&&(o.children.css({position:"absolute",zIndex:0,display:"none"}),o.children.eq(o.settings.startSlide).css({zIndex:o.settings.slideZIndex,display:"block"})),o.controls.el=t('<div class="bx-controls" />'),o.settings.captions&&P(),o.active.last=o.settings.startSlide==x()-1,o.settings.video&&r.fitVids();var e=o.children.eq(o.settings.startSlide);"all"==o.settings.preloadImages&&(e=o.children),o.settings.ticker?o.settings.pager=!1:(o.settings.pager&&T(),o.settings.controls&&C(),o.settings.auto&&o.settings.autoControls&&E(),(o.settings.controls||o.settings.autoControls||o.settings.pager)&&o.viewport.after(o.controls.el)),g(e,h)},g=function(e,i){var s=e.find("img, iframe").length;if(0==s)return i(),void 0;var n=0;e.find("img, iframe").each(function(){t(this).one("load",function(){++n==s&&i()}).each(function(){this.complete&&t(this).load()})})},h=function(){if(o.settings.infiniteLoop&&"fade"!=o.settings.mode&&!o.settings.ticker){var e="vertical"==o.settings.mode?o.settings.minSlides:o.settings.maxSlides,i=o.children.slice(0,e).clone().addClass("bx-clone"),s=o.children.slice(-e).clone().addClass("bx-clone");r.append(i).prepend(s)}o.loader.remove(),S(),"vertical"==o.settings.mode&&(o.settings.adaptiveHeight=!0),o.viewport.height(v()),r.redrawSlider(),o.settings.onSliderLoad(o.active.index),o.initialized=!0,o.settings.responsive&&t(window).bind("resize",Z),o.settings.auto&&o.settings.autoStart&&H(),o.settings.ticker&&L(),o.settings.pager&&q(o.settings.startSlide),o.settings.controls&&W(),o.settings.touchEnabled&&!o.settings.ticker&&O()},v=function(){var e=0,s=t();if("vertical"==o.settings.mode||o.settings.adaptiveHeight)if(o.carousel){var n=1==o.settings.moveSlides?o.active.index:o.active.index*m();for(s=o.children.eq(n),i=1;i<=o.settings.maxSlides-1;i++)s=n+i>=o.children.length?s.add(o.children.eq(i-1)):s.add(o.children.eq(n+i))}else s=o.children.eq(o.active.index);else s=o.children;return"vertical"==o.settings.mode?(s.each(function(){e+=t(this).outerHeight()}),o.settings.slideMargin>0&&(e+=o.settings.slideMargin*(o.settings.minSlides-1))):e=Math.max.apply(Math,s.map(function(){return t(this).outerHeight(!1)}).get()),e},p=function(){var t="100%";return o.settings.slideWidth>0&&(t="horizontal"==o.settings.mode?o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin:o.settings.slideWidth),t},u=function(){var t=o.settings.slideWidth,e=o.viewport.width();return 0==o.settings.slideWidth||o.settings.slideWidth>e&&!o.carousel||"vertical"==o.settings.mode?t=e:o.settings.maxSlides>1&&"horizontal"==o.settings.mode&&(e>o.maxThreshold||e<o.minThreshold&&(t=(e-o.settings.slideMargin*(o.settings.minSlides-1))/o.settings.minSlides)),t},f=function(){var t=1;if("horizontal"==o.settings.mode&&o.settings.slideWidth>0)if(o.viewport.width()<o.minThreshold)t=o.settings.minSlides;else if(o.viewport.width()>o.maxThreshold)t=o.settings.maxSlides;else{var e=o.children.first().width();t=Math.floor(o.viewport.width()/e)}else"vertical"==o.settings.mode&&(t=o.settings.minSlides);return t},x=function(){var t=0;if(o.settings.moveSlides>0)if(o.settings.infiniteLoop)t=o.children.length/m();else for(var e=0,i=0;e<o.children.length;)++t,e=i+f(),i+=o.settings.moveSlides<=f()?o.settings.moveSlides:f();else t=Math.ceil(o.children.length/f());return t},m=function(){return o.settings.moveSlides>0&&o.settings.moveSlides<=f()?o.settings.moveSlides:f()},S=function(){if(o.children.length>o.settings.maxSlides&&o.active.last&&!o.settings.infiniteLoop){if("horizontal"==o.settings.mode){var t=o.children.last(),e=t.position();b(-(e.left-(o.viewport.width()-t.width())),"reset",0)}else if("vertical"==o.settings.mode){var i=o.children.length-o.settings.minSlides,e=o.children.eq(i).position();b(-e.top,"reset",0)}}else{var e=o.children.eq(o.active.index*m()).position();o.active.index==x()-1&&(o.active.last=!0),void 0!=e&&("horizontal"==o.settings.mode?b(-e.left,"reset",0):"vertical"==o.settings.mode&&b(-e.top,"reset",0))}},b=function(t,e,i,s){if(o.usingCSS){var n="vertical"==o.settings.mode?"translate3d(0, "+t+"px, 0)":"translate3d("+t+"px, 0, 0)";r.css("-"+o.cssPrefix+"-transition-duration",i/1e3+"s"),"slide"==e?(r.css(o.animProp,n),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),D()})):"reset"==e?r.css(o.animProp,n):"ticker"==e&&(r.css("-"+o.cssPrefix+"-transition-timing-function","linear"),r.css(o.animProp,n),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),b(s.resetValue,"reset",0),N()}))}else{var a={};a[o.animProp]=t,"slide"==e?r.animate(a,i,o.settings.easing,function(){D()}):"reset"==e?r.css(o.animProp,t):"ticker"==e&&r.animate(a,speed,"linear",function(){b(s.resetValue,"reset",0),N()})}},w=function(){for(var e="",i=x(),s=0;i>s;s++){var n="";o.settings.buildPager&&t.isFunction(o.settings.buildPager)?(n=o.settings.buildPager(s),o.pagerEl.addClass("bx-custom-pager")):(n=s+1,o.pagerEl.addClass("bx-default-pager")),e+='<div class="bx-pager-item"><a href="" data-slide-index="'+s+'" class="bx-pager-link">'+n+"</a></div>"}o.pagerEl.html(e)},T=function(){o.settings.pagerCustom?o.pagerEl=t(o.settings.pagerCustom):(o.pagerEl=t('<div class="bx-pager" />'),o.settings.pagerSelector?t(o.settings.pagerSelector).html(o.pagerEl):o.controls.el.addClass("bx-has-pager").append(o.pagerEl),w()),o.pagerEl.on("click","a",I)},C=function(){o.controls.next=t('<a class="bx-next" href="">'+o.settings.nextText+"</a>"),o.controls.prev=t('<a class="bx-prev" href="">'+o.settings.prevText+"</a>"),o.controls.next.bind("click",y),o.controls.prev.bind("click",z),o.settings.nextSelector&&t(o.settings.nextSelector).append(o.controls.next),o.settings.prevSelector&&t(o.settings.prevSelector).append(o.controls.prev),o.settings.nextSelector||o.settings.prevSelector||(o.controls.directionEl=t('<div class="bx-controls-direction" />'),o.controls.directionEl.append(o.controls.prev).append(o.controls.next),o.controls.el.addClass("bx-has-controls-direction").append(o.controls.directionEl))},E=function(){o.controls.start=t('<div class="bx-controls-auto-item"><a class="bx-start" href="">'+o.settings.startText+"</a></div>"),o.controls.stop=t('<div class="bx-controls-auto-item"><a class="bx-stop" href="">'+o.settings.stopText+"</a></div>"),o.controls.autoEl=t('<div class="bx-controls-auto" />'),o.controls.autoEl.on("click",".bx-start",k),o.controls.autoEl.on("click",".bx-stop",M),o.settings.autoControlsCombine?o.controls.autoEl.append(o.controls.start):o.controls.autoEl.append(o.controls.start).append(o.controls.stop),o.settings.autoControlsSelector?t(o.settings.autoControlsSelector).html(o.controls.autoEl):o.controls.el.addClass("bx-has-controls-auto").append(o.controls.autoEl),A(o.settings.autoStart?"stop":"start")},P=function(){o.children.each(function(){var e=t(this).find("img:first").attr("title");void 0!=e&&(""+e).length&&t(this).append('<div class="bx-caption"><span>'+e+"</span></div>")})},y=function(t){o.settings.auto&&r.stopAuto(),r.goToNextSlide(),t.preventDefault()},z=function(t){o.settings.auto&&r.stopAuto(),r.goToPrevSlide(),t.preventDefault()},k=function(t){r.startAuto(),t.preventDefault()},M=function(t){r.stopAuto(),t.preventDefault()},I=function(e){o.settings.auto&&r.stopAuto();var i=t(e.currentTarget),s=parseInt(i.attr("data-slide-index"));s!=o.active.index&&r.goToSlide(s),e.preventDefault()},q=function(e){var i=o.children.length;return"short"==o.settings.pagerType?(o.settings.maxSlides>1&&(i=Math.ceil(o.children.length/o.settings.maxSlides)),o.pagerEl.html(e+1+o.settings.pagerShortSeparator+i),void 0):(o.pagerEl.find("a").removeClass("active"),o.pagerEl.each(function(i,s){t(s).find("a").eq(e).addClass("active")}),void 0)},D=function(){if(o.settings.infiniteLoop){var t="";0==o.active.index?t=o.children.eq(0).position():o.active.index==x()-1&&o.carousel?t=o.children.eq((x()-1)*m()).position():o.active.index==o.children.length-1&&(t=o.children.eq(o.children.length-1).position()),t&&("horizontal"==o.settings.mode?b(-t.left,"reset",0):"vertical"==o.settings.mode&&b(-t.top,"reset",0))}o.working=!1,o.settings.onSlideAfter(o.children.eq(o.active.index),o.oldIndex,o.active.index)},A=function(t){o.settings.autoControlsCombine?o.controls.autoEl.html(o.controls[t]):(o.controls.autoEl.find("a").removeClass("active"),o.controls.autoEl.find("a:not(.bx-"+t+")").addClass("active"))},W=function(){1==x()?(o.controls.prev.addClass("disabled"),o.controls.next.addClass("disabled")):!o.settings.infiniteLoop&&o.settings.hideControlOnEnd&&(0==o.active.index?(o.controls.prev.addClass("disabled"),o.controls.next.removeClass("disabled")):o.active.index==x()-1?(o.controls.next.addClass("disabled"),o.controls.prev.removeClass("disabled")):(o.controls.prev.removeClass("disabled"),o.controls.next.removeClass("disabled")))},H=function(){o.settings.autoDelay>0?setTimeout(r.startAuto,o.settings.autoDelay):r.startAuto(),o.settings.autoHover&&r.hover(function(){o.interval&&(r.stopAuto(!0),o.autoPaused=!0)},function(){o.autoPaused&&(r.startAuto(!0),o.autoPaused=null)})},L=function(){var e=0;if("next"==o.settings.autoDirection)r.append(o.children.clone().addClass("bx-clone"));else{r.prepend(o.children.clone().addClass("bx-clone"));var i=o.children.first().position();e="horizontal"==o.settings.mode?-i.left:-i.top}b(e,"reset",0),o.settings.pager=!1,o.settings.controls=!1,o.settings.autoControls=!1,o.settings.tickerHover&&!o.usingCSS&&o.viewport.hover(function(){r.stop()},function(){var e=0;o.children.each(function(){e+="horizontal"==o.settings.mode?t(this).outerWidth(!0):t(this).outerHeight(!0)});var i=o.settings.speed/e,s="horizontal"==o.settings.mode?"left":"top",n=i*(e-Math.abs(parseInt(r.css(s))));N(n)}),N()},N=function(t){speed=t?t:o.settings.speed;var e={left:0,top:0},i={left:0,top:0};"next"==o.settings.autoDirection?e=r.find(".bx-clone").first().position():i=o.children.first().position();var s="horizontal"==o.settings.mode?-e.left:-e.top,n="horizontal"==o.settings.mode?-i.left:-i.top,a={resetValue:n};b(s,"ticker",speed,a)},O=function(){o.touch={start:{x:0,y:0},end:{x:0,y:0}},o.viewport.bind("touchstart",X)},X=function(t){if(o.working)t.preventDefault();else{o.touch.originalPos=r.position();var e=t.originalEvent;o.touch.start.x=e.changedTouches[0].pageX,o.touch.start.y=e.changedTouches[0].pageY,o.viewport.bind("touchmove",Y),o.viewport.bind("touchend",V)}},Y=function(t){var e=t.originalEvent,i=Math.abs(e.changedTouches[0].pageX-o.touch.start.x),s=Math.abs(e.changedTouches[0].pageY-o.touch.start.y);if(3*i>s&&o.settings.preventDefaultSwipeX?t.preventDefault():3*s>i&&o.settings.preventDefaultSwipeY&&t.preventDefault(),"fade"!=o.settings.mode&&o.settings.oneToOneTouch){var n=0;if("horizontal"==o.settings.mode){var r=e.changedTouches[0].pageX-o.touch.start.x;n=o.touch.originalPos.left+r}else{var r=e.changedTouches[0].pageY-o.touch.start.y;n=o.touch.originalPos.top+r}b(n,"reset",0)}},V=function(t){o.viewport.unbind("touchmove",Y);var e=t.originalEvent,i=0;if(o.touch.end.x=e.changedTouches[0].pageX,o.touch.end.y=e.changedTouches[0].pageY,"fade"==o.settings.mode){var s=Math.abs(o.touch.start.x-o.touch.end.x);s>=o.settings.swipeThreshold&&(o.touch.start.x>o.touch.end.x?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto())}else{var s=0;"horizontal"==o.settings.mode?(s=o.touch.end.x-o.touch.start.x,i=o.touch.originalPos.left):(s=o.touch.end.y-o.touch.start.y,i=o.touch.originalPos.top),!o.settings.infiniteLoop&&(0==o.active.index&&s>0||o.active.last&&0>s)?b(i,"reset",200):Math.abs(s)>=o.settings.swipeThreshold?(0>s?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto()):b(i,"reset",200)}o.viewport.unbind("touchend",V)},Z=function(){var e=t(window).width(),i=t(window).height();(a!=e||l!=i)&&(a=e,l=i,r.redrawSlider(),o.settings.onSliderResize.call(r,o.active.index))};return r.goToSlide=function(e,i){if(!o.working&&o.active.index!=e)if(o.working=!0,o.oldIndex=o.active.index,o.active.index=0>e?x()-1:e>=x()?0:e,o.settings.onSlideBefore(o.children.eq(o.active.index),o.oldIndex,o.active.index),"next"==i?o.settings.onSlideNext(o.children.eq(o.active.index),o.oldIndex,o.active.index):"prev"==i&&o.settings.onSlidePrev(o.children.eq(o.active.index),o.oldIndex,o.active.index),o.active.last=o.active.index>=x()-1,o.settings.pager&&q(o.active.index),o.settings.controls&&W(),"fade"==o.settings.mode)o.settings.adaptiveHeight&&o.viewport.height()!=v()&&o.viewport.animate({height:v()},o.settings.adaptiveHeightSpeed),o.children.filter(":visible").fadeOut(o.settings.speed).css({zIndex:0}),o.children.eq(o.active.index).css("zIndex",o.settings.slideZIndex+1).fadeIn(o.settings.speed,function(){t(this).css("zIndex",o.settings.slideZIndex),D()});else{o.settings.adaptiveHeight&&o.viewport.height()!=v()&&o.viewport.animate({height:v()},o.settings.adaptiveHeightSpeed);var s=0,n={left:0,top:0};if(!o.settings.infiniteLoop&&o.carousel&&o.active.last)if("horizontal"==o.settings.mode){var a=o.children.eq(o.children.length-1);n=a.position(),s=o.viewport.width()-a.outerWidth()}else{var l=o.children.length-o.settings.minSlides;n=o.children.eq(l).position()}else if(o.carousel&&o.active.last&&"prev"==i){var d=1==o.settings.moveSlides?o.settings.maxSlides-m():(x()-1)*m()-(o.children.length-o.settings.maxSlides),a=r.children(".bx-clone").eq(d);n=a.position()}else if("next"==i&&0==o.active.index)n=r.find("> .bx-clone").eq(o.settings.maxSlides).position(),o.active.last=!1;else if(e>=0){var c=e*m();n=o.children.eq(c).position()}if("undefined"!=typeof n){var g="horizontal"==o.settings.mode?-(n.left-s):-n.top;b(g,"slide",o.settings.speed)}}},r.goToNextSlide=function(){if(o.settings.infiniteLoop||!o.active.last){var t=parseInt(o.active.index)+1;r.goToSlide(t,"next")}},r.goToPrevSlide=function(){if(o.settings.infiniteLoop||0!=o.active.index){var t=parseInt(o.active.index)-1;r.goToSlide(t,"prev")}},r.startAuto=function(t){o.interval||(o.interval=setInterval(function(){"next"==o.settings.autoDirection?r.goToNextSlide():r.goToPrevSlide()},o.settings.pause),o.settings.autoControls&&1!=t&&A("stop"))},r.stopAuto=function(t){o.interval&&(clearInterval(o.interval),o.interval=null,o.settings.autoControls&&1!=t&&A("start"))},r.getCurrentSlide=function(){return o.active.index},r.getCurrentSlideElement=function(){return o.children.eq(o.active.index)},r.getSlideCount=function(){return o.children.length},r.redrawSlider=function(){o.children.add(r.find(".bx-clone")).outerWidth(u()),o.viewport.css("height",v()),o.settings.ticker||S(),o.active.last&&(o.active.index=x()-1),o.active.index>=x()&&(o.active.last=!0),o.settings.pager&&!o.settings.pagerCustom&&(w(),q(o.active.index))},r.destroySlider=function(){o.initialized&&(o.initialized=!1,t(".bx-clone",this).remove(),o.children.each(function(){void 0!=t(this).data("origStyle")?t(this).attr("style",t(this).data("origStyle")):t(this).removeAttr("style")}),void 0!=t(this).data("origStyle")?this.attr("style",t(this).data("origStyle")):t(this).removeAttr("style"),t(this).unwrap().unwrap(),o.controls.el&&o.controls.el.remove(),o.controls.next&&o.controls.next.remove(),o.controls.prev&&o.controls.prev.remove(),o.pagerEl&&o.settings.controls&&o.pagerEl.remove(),t(".bx-caption",this).remove(),o.controls.autoEl&&o.controls.autoEl.remove(),clearInterval(o.interval),o.settings.responsive&&t(window).unbind("resize",Z))},r.reloadSlider=function(t){void 0!=t&&(n=t),r.destroySlider(),d()},d(),this}}(jQuery);
		</script>
	</head>
	<body class="landing-page-template-default single single-landing-page postid-1580 desktop chrome" >
        <div class="flex-container">

<form id="sliderForm" action="https://fastcapital360.secure.force.com/apply" method="POST" class="slider-form" onSubmit="" >
<ul class="bxslider">
    <li>

        <div id="tracking-question-1" style="padding-left:20px;" class="ah-form-container">
            <div class="inner-container">
				<span style="color:#263357; font-size:24px; font-family: 'Roboto',serif;"><center>Business Qualification Question 1 of 3</center></span>
                <span style="color:#263357; font-size:30px; font-weight:700; font-family: 'Roboto',serif;"> <center>What is your average monthly revenue? </center></span>
                </div>

            <div class="container-fluid">
                <div class="ah-checkbox-container">
                    <input type="radio" id="q5bo1" name="monthly_revenue" value="Less than $5,000"/>
                    <label onclick="nextSlide()" class="hvr-grow" for="q5bo1"><span></span>Less than $5,000</label>
                </div>
                <div class="ah-checkbox-container ">
                    <input type="radio" id="q5bo2" name="monthly_revenue" value="$5,001 - $10,000"/>
                    <label onclick="nextSlide()" class="hvr-grow" for="q5bo2"><span></span>$5,001 - $10,000</label>
                </div>
                <div class="ah-checkbox-container ">
                    <input type="radio" id="q5bo3" name="monthly_revenue" value="$10,001 - $25,000"/>
                    <label onclick="nextSlide()" class="hvr-grow" for="q5bo3"><span></span>$10,001 - $25,000</label>
                </div>
                <div class="ah-checkbox-container ">
                    <input type="radio" id="q5bo4" name="monthly_revenue" value="$25,001 - $50,000"/>
                    <label onclick="nextSlide()" class="hvr-grow" for="q5bo4"><span></span>$25,001 - $50,000</label>
                </div>
                <div class="ah-checkbox-container ">
                    <input type="radio" id="q5bo5" name="monthly_revenue" value="$50,001 - $100,000"/>
                    <label onclick="nextSlide()" class="hvr-grow" for="q5bo5"><span></span>$50,001 - $100,000</label>
                </div>
                <div class="ah-checkbox-container ">
                    <input type="radio" id="q5bo6" name="monthly_revenue" value="$100,001 - $250,000"/>
                    <label onclick="nextSlide()" class="hvr-grow" for="q5bo6"><span></span>$100,001 - $250,000</label>
                </div>
                <div class="ah-checkbox-container ">
                    <input type="radio" id="q5bo7" name="monthly_revenue" value="$250,001 +"/>
                    <label onclick="nextSlide()" class="hvr-grow" for="q5bo7"><span></span>$250,001 +</label>
                </div>
            </div>
         </div>

    </li><!-- End item 1 -->
    <li>
     <div id="tracking-question-3" style="padding-left:20px;" class="ah-form-container">
                <div class="ah-btn-container">
          
                    			<span style="color:#263357; font-size:24px; font-family: 'Roboto',serif;"><center>Business Qualification Question 2 of 3</center></span>
                <span style="color:#263357; font-size:30px; font-weight:700; font-family: 'Roboto',serif;"> <center>What is your average monthly revenue? </center></span>
                </div>
            <div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="how_long_in_business" value="Less than 3 months" id="q3o1" />
                    <label onclick="nextSlide()" class="hvr-grow"  for="q3o1"><span></span>Less than 3 months</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="how_long_in_business" value="3 - 5 months" id="q3o2" />
                    <label onclick="nextSlide()" class="hvr-grow" for="q3o2"><span></span>3 - 5 months</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="how_long_in_business" value="6 - 11 months" id="q3o3" />
                    <label onclick="nextSlide()" class="hvr-grow" for="q3o3"><span></span>6 - 11 months</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="how_long_in_business" value="1 - 2 years" id="q3o4" />
                    <label onclick="nextSlide()" class="hvr-grow" for="q3o4"><span></span>1 - 2 years</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="how_long_in_business" value="3 - 5 years" id="q3o5" />
                    <label onclick="nextSlide()" class="hvr-grow" for="q3o5"><span></span>3 - 5 years</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="how_long_in_business" value="More than 5 years" id="q3o6" />
                    <label onclick="nextSlide()" class="hvr-grow" for="q3o6"><span></span>More than 5 years</label>
                </div>
            </div>

         </div>
    </li><!-- End item 3 -->
    <li>
     <div id="tracking-question-4" style="padding-left:20px;" class="ah-form-container">
                <div class="ah-btn-container">
              
                    			<span style="color:#263357; font-size:24px; font-family: 'Roboto',serif;"><center>Business Qualification Question 3 of 3</center></span>
                <span style="color:#263357; font-size:30px; font-weight:700; font-family: 'Roboto',serif;"> <center>What is your average monthly revenue? </center></span>
                </div>

            <div >
                <div class="ah-checkbox-container">
                    <input type="radio" name="credit_score"  value="Less than 550" id="q4o1" />
                    <label onclick="nextSlide()" class="hvr-grow"  for="q4o1"><span></span>Less than 550</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="credit_score" value="550 - 600" id="q4o2" />
                    <label onclick="nextSlide()" class="hvr-grow" for="q4o2"><span></span>550 - 600</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="credit_score" value="600 - 650" id="q4o3" />
                    <label onclick="nextSlide()" class="hvr-grow"  for="q4o3"><span></span>600 - 650</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="credit_score" value="650 - 700" id="q4o4" />
                    <label onclick="nextSlide()" class="hvr-grow" for="q4o4"><span></span>650 - 700</label>
                </div>
                <div class="ah-checkbox-container">
                    <input type="radio" name="credit_score" value="700+" id="q4o5" />
                    <label onclick="nextSlide()" class="hvr-grow" for="q4o5"><span></span>Higher than 700</label>
                </div>
            </div>

         </div>

    </li>
    <li>
          <div id="tracking-question-7" class="ah-form-container input-form">
                <div class="ah-slider-left">
                    <div class="ah-btn-container">
                   			<span style="color:#263357; font-size:24px; font-family: 'Roboto',serif;"><center>Business Qualification Question 1 of 3</center></span>
                <span style="color:#263357; font-size:30px; font-weight:700; font-family: 'Roboto',serif;"> <center>What is your average monthly revenue? </center></span>
                    </div>
                </div>

                <div class=" ah-end-form ">

                        <input class="ah-half-width" onfocus="if (this.value=='First Name') this.value = ''" onblur="if (this.value=='') this.value = 'First Name'" id="first_name" value="First Name" maxlength="40" name="first_name" size="20" type="text" /><br>
                         <input class="ah-half-width ah-right-input" onfocus="if (this.value=='Last Name') this.value = ''" onblur="if (this.value=='') this.value = 'Last Name'" value="Last Name" id="last_name" maxlength="80" name="last_name" size="20" type="text" /><br>
                        <input class="ah-half-width" onfocus="if (this.value=='Company Name') this.value = ''" onblur="if (this.value=='') this.value = 'Company Name'" value="Company Name" id="Company" maxlength="40" name="Company" size="20" type="text" required /><br>
                        <select class="ah-half-width ah-right-input home-head" onfocus="if (this.value=='Industry') this.value = ''" onblur="if (this.value=='') this.value = 'Industry'" value="Industry" id="industry" maxlength="80" name="industry" required/>
                            <option value="" disabled selected>Select your industry...</option>
                            <option>Administrative and Support Services</option>
                            <option>Agriculture</option>
                            <option>Arts, Entertainment, and Recreation</option>
                            <option>Automobile Dealers</option>
                            <option>Automotive Repair and Maintenance</option>
                            <option>Business Services</option>
                            <option>Commercial Cleaning Services</option>
                            <option>Computer Systems and Programming</option>
                            <option>Construction</option>
                            <option>Consumer Services</option>
                            <option>Educational Services</option>
                            <option>Finance and Insurance</option>
                            <option>Forestry, Fishing, and Hunting</option>
                            <option>Franchise</option>
                            <option>Freight Trucking</option>
                            <option>Gambling Industries</option>
                            <option>Gas Stations</option>
                            <option>Greenhouse, Nursery, and Floriculture</option>
                            <option>Healthcare and Social Assistance</option>
                            <option>Hotels and Travel Accomodations</option>
                            <option>Information and Technology</option>
                            <option>Legal Services</option>
                            <option>Management of Companies</option>
                            <option>Manufacturing</option>
                            <option>Marketing Consulting Services</option>
                            <option>Mining</option>
                            <option>Non-Profit Organizations</option>
                            <option>Other</option>
                            <option>Other Services</option>
                            <option>Personal Care Services</option>
                            <option>Physician, Dentist or Health Doctor</option>
                            <option>Professional, Scientific & Tech Services</option>
                            <option>Public Administration</option>
                            <option>Real Estate</option>
                            <option>Religious Institutions</option>
                            <option>Rental and Leasing</option>
                            <option>Restaurants and Food Services</option>
                            <option>Retail Stores</option>
                            <option>Transportation</option>
                            <option>Transportation & Warehousing</option>
                            <option>Utilities</option>
                            <option>Veterinarians</option>
                            <option>Warehousing and Storage</option>
                            <option>Waste Management & Remediation Services</option>
                            <option>Wholesale Products</option>
                            <option>Wholesale Trade</option>
                        </select><br>
                        <input onfocus="if (this.value=='Email Address') this.value = ''" onblur="if (this.value=='') this.value = 'Email Address'" type="email" value="Email Address" id="email" maxlength="80" name="email" size="20" required /><br>
                         <input class="ah-half-width" onfocus="if (this.value=='Phone Number') this.value = ''" onblur="if (this.value=='') this.value = 'Phone Number'" value="Phone Number" id="phone" name="phone" pattern="(?:\(\d{3}\)|\d{3})[- ]?\d{3}[- ]?\d{4}"  size="20" type="tel" required /><br>
                        <input class="ah-half-width ah-right-input" onfocus="if (this.value=='Zip Code') this.value = ''" onblur="if (this.value=='') this.value = 'Zip Code'" value="Zip Code" id="zip" maxlength="20" name="zip" size="20" type="text" required /><br>
                        <div class="lead-form-check">
                           

                        </div>
                        <input type="submit" value="Get your quote" name="submitButton"/>

                </div>

             </div>

    </li><!-- End item 6 -->
</ul>


</form><!-- End main form -->


<!-- Call the slider -->
<script type="text/javascript">
//Validate and send form
$(document).ready(function(){
  $.validator.addMethod('notEquals', function(value, element, string) {
        return value !== string;
  }, $.validator.format("Please enter a valid name"));
  $('#sliderForm').validate({
      submitHandler: function (form){
        //nextSlide();
        //Page now redirects to thank you page.
        form.submit();
      },
      errorElement: 'div',
      rules: {
          phone: {
              phoneUS: true
          },
          first_name:{
              notEquals: 'First Name'
          },
          last_name: {
              notEquals: 'Last Name'
          },
          Company: {
              notEquals: 'Company Name'
          }
      },
      messages: {
          phone: "Please enter a valid phone number"
      }
  });

  $('.bxslider').bxSlider({
    touchEnabled:false,
    onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
        $('.active-slide').removeClass('active-slide');
        $('.bxslider>li').eq(currentSlideHtmlObject).addClass('active-slide')
//        var formHeight = $('.active-slide > .ah-form-container').height();
//        $('#sliderForm').css('height', formHeight + 45);
//        console.log(formHeight);
        },
      onSliderLoad: function () {
        $('.bxslider>li').eq(0).addClass('active-slide')
//        $('#sliderForm').css('height', '543');
      }
  });
});
</script>

</div>
  </body>
</html>