if (isset($_GET['pid'])){
  $pid = $urlcrypt->decrypt($_GET['pid']);
}

$jsonupsells = $users->get_upsells_ajax($pid);

$indexedOnly = $jsonupsells;

foreach ($associative as $row) {
  $indexedOnly[] = array_values($row);
}

return json_encode($indexedOnly);