<?php
/*
Plugin Name: Contact Form 7 - Salesforce CRM
Description: This plugin can integrate Contacts, Cases and Leads between your WordPress Contact Form 7 and Salesforce CRM. Easily add automatically Contacts, Cases and Leads into Salesforce CRM when people submit a Contact Form 7 form on your site.
Version:     1.3.0
Author:      Obtain Code
Author URI:  http://obtaincode.com/
License:     GPL2
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit( 'restricted access' );
}

define( 'CF7_SF_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

/*
 * This is a class file for Salesforce CRM API
 */
include_once( 'includes/class-salesforce.php' );

/*
 * This is a core functions file
 */
include_once( 'includes/functions.php' );

/*
 * This is a integration file
 */
include_once( 'admin/integration.php' );

/*
 * This is a configuration file
 */
include_once( 'admin/configuration.php' );

/*
 * This is a function that run during active plugin
 */
if ( ! function_exists( 'cf7_sf_activation' ) ) {
    register_activation_hook( __FILE__, 'cf7_sf_activation' );
    function cf7_sf_activation() {
        
        update_option( 'cf7_sf_modules', 'a:3:{s:4:"Case";s:4:"Case";s:7:"Contact";s:7:"Contact";s:4:"Lead";s:4:"Lead";}' );
    }
}

/*
 * This is a function that integrate form
 * $cf7 variable return form data
 */
if ( ! function_exists( 'cf7_sf_integration' ) ) {
    add_action( 'wpcf7_before_send_mail', 'cf7_sf_integration', 20, 1 );
    function cf7_sf_integration( $cf7 ) {
        
        $licence = get_site_option( 'cf7_sf_licence' );
        if ( $licence ) {
            $submission = WPCF7_Submission::get_instance();
            if ( $submission ) {
                $_REQUEST = $submission->get_posted_data();     
            }

            $form_id = 0;
            if ( isset( $_REQUEST['_wpcf7'] ) ) {
                $form_id = intval( $_REQUEST['_wpcf7'] );
            }
            
            if ( $form_id ) {
                $cf7_sf = get_post_meta( $form_id, 'cf7_sf', true );
                if ( $cf7_sf ) {
                    $cf7_sf_fields = get_post_meta( $form_id, 'cf7_sf_fields', true );
                    if ( $cf7_sf_fields != null ) {                    
                        $cf7_sf_data = array();
                        foreach ( $cf7_sf_fields as $cf7_sf_field_key => $cf7_sf_field ) {
                            if ( isset( $cf7_sf_field[0] ) ) {
                                $cf7_sf_field['key'] = $cf7_sf_field[0];
                            }

                            if ( isset( $cf7_sf_field['key'] ) && $cf7_sf_field['key'] ) {
                                if ( is_array( $_REQUEST[$cf7_sf_field_key] ) ) {
                                    $_REQUEST[$cf7_sf_field_key] = implode( ';', $_REQUEST[$cf7_sf_field_key] );
                                }

                                if ( isset( $cf7_sf_field['type'] ) && $cf7_sf_field['type'] == 'boolean' ) {
                                    if ( $_REQUEST[$cf7_sf_field_key] == '1' || $_REQUEST[$cf7_sf_field_key] == 'true' ) {
                                        $_REQUEST[$cf7_sf_field_key] = 'true';
                                    } else {
                                        $_REQUEST[$cf7_sf_field_key] = 'false';
                                    }
                                }

                                $cf7_sf_data[$cf7_sf_field['key']] = strip_tags( $_REQUEST[$cf7_sf_field_key] );
                            }
                        }
                        
                        if ( $cf7_sf_data != null ) {
                            $client_id = get_option( 'cf7_sf_client_id' );
                            $client_secret = get_option( 'cf7_sf_client_secret' );
                            $username = get_option( 'cf7_sf_username' );
                            $password = cf7_sf_crypt( get_option( 'cf7_sf_password' ), 'd', $client_secret );

                            $cf7_sf = new CF7_SF_REST_API( $client_id, $client_secret, $username, $password );
                            $authentication = $cf7_sf->authentication();
                            if ( ! isset( $authentication->error ) ) { 
                                $cf7_sf_module = get_post_meta( $form_id, 'cf7_sf_module', true );
                                $cf7_sf->addRecord( $authentication->instance_url, $authentication->token_type, $authentication->access_token, $cf7_sf_module, $cf7_sf_data, $form_id );
                            }
                        }
                    }
                }
            }
        }
    }
}