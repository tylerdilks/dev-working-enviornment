<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'restricted access' );
}

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    die;
}

/*
 * Delete options when plugin uninstall
 */
delete_option( 'cf7_sf_client_id' );
delete_option( 'cf7_sf_client_secret' );
delete_option( 'cf7_sf_username' );
delete_option( 'cf7_sf_password' );
delete_option( 'cf7_sf_modules' );
delete_option( 'cf7_sf_modules_fields' );