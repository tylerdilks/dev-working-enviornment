<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'restricted access' );
}

/*
 * This is a function that create menu
 */
if ( ! function_exists( 'cf7_sf_add_integration_main_menu' ) ) {
    add_action('admin_menu', 'cf7_sf_add_integration_main_menu');
    function cf7_sf_add_integration_main_menu() {
        
        add_menu_page( 'Contact Form 7 Salesforce Integration', 'CF7 - Salesforce', 'manage_options', 'cf7_sf-integration', 'cf7_sf_integration_main_menu_callback', 'dashicons-migrate' );
        add_submenu_page( 'cf7_sf-integration', 'Contact Form 7 Salesforce Integration', 'Integration', 'manage_options', 'cf7_sf-integration', 'cf7_sf_integration_main_menu_callback' );
    }
}

/*
 * This is a function for integration
 */
if ( ! function_exists( 'cf7_sf_integration_main_menu_callback' ) ) {
    function cf7_sf_integration_main_menu_callback() {
        
        $cf7_sf_licence = get_site_option( 'cf7_sf_licence' );
        ?>
            <div class="wrap">                
                <h1><?php _e( 'Contact Form 7 Salesforce Integration' ); ?></h1>
                <hr>                
                <?php
                    if ( $cf7_sf_licence ) {
                        if ( isset( $_REQUEST['id'] ) ) {
                            $id = intval( $_REQUEST['id'] );
                            if ( isset( $_REQUEST['submit'] ) ) {
                                update_post_meta( $id, 'cf7_sf', $_REQUEST['cf7_sf'] );
                                update_post_meta( $id, 'cf7_sf_fields', $_REQUEST['cf7_sf_fields'] );  
                                ?>
                                    <div class="notice notice-success is-dismissible">
                                        <p><?php _e( 'Integration settings saved.' ); ?></p>
                                    </div>
                                <?php
                            } else if ( isset( $_REQUEST['filter'] ) ) { 
                                update_post_meta( $id, 'cf7_sf_module', $_REQUEST['cf7_sf_module'] );
                            }

                            $cf7_sf_module = get_post_meta( $id, 'cf7_sf_module', true );

                            $cf7_sf = get_post_meta( $id, 'cf7_sf', true );
                            $cf7_sf_fields = get_post_meta( $id, 'cf7_sf_fields', true );                        
                            ?>   
                                <h2><?php _e( 'Form' ); ?>: <?php echo get_the_title( $id ); ?></h2>
                                <hr>
                                <form method="post">
                                    <table class="form-table">
                                        <tbody>
                                            <tr>
                                                <th scope="row"><label><?php _e( 'Module' ); ?></label></th>
                                                <td>
                                                    <select name="cf7_sf_module">
                                                        <option value=""><?php _e( 'Select a module' ); ?></option>
                                                        <?php
                                                            $modules = unserialize( get_option( 'cf7_sf_modules' ) );
                                                            foreach ( $modules as $key => $value ) {
                                                                $selected = '';
                                                                if ( $key == $cf7_sf_module ) {
                                                                    $selected = ' selected="selected"';
                                                                }
                                                                ?>
                                                                    <option value="<?php echo $key; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?php _e( 'Filter module fields' ); ?></th>
                                                <td><button type="submit" name="filter" class='button-secondary'><?php _e( 'Filter' ); ?></button></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><label><?php _e( 'Salesforce Integration?' ); ?></label></th>
                                                <td>
                                                    <input type="hidden" name="cf7_sf" value="0" />
                                                    <input type="checkbox" name="cf7_sf" value="1"<?php echo ( $cf7_sf ? ' checked' : '' ); ?> />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php
                                        $_form = get_post_meta( $id, '_form', true );
                                        if ( $_form ) {
                                            preg_match_all( '#\[(.*?)\]#', $_form, $matches );
                                            $cf7_fields = array();
                                            if ( $matches != null ) {
                                                foreach ( $matches[1] as $match ) {
                                                    $match_explode = explode( ' ', $match );
                                                    $field_type = str_replace( '*', '', $match_explode[0] );
                                                    if ( $field_type != 'submit' ) {
                                                        if ( isset( $match_explode[1] ) ) {
                                                            $cf7_fields[$match_explode[1]] = array(
                                                                'key'   => $match_explode[1],
                                                                'type'  => $field_type,
                                                            );
                                                        }
                                                    }
                                                }

                                                if ( $cf7_fields != null ) {
                                                    ?>
                                                        <table class="widefat striped">
                                                            <thead>
                                                                <tr>
                                                                    <th><?php _e( 'Contact Form 7 Field' ); ?></th>
                                                                    <th><?php _e( 'Salesforce Field' ); ?></th>
                                                                </tr>
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th><?php _e( 'Contact Form 7 Field' ); ?></th>
                                                                    <th><?php _e( 'Salesforce Field' ); ?></th>
                                                                </tr>
                                                            </tfoot>
                                                            <tbody>
                                                                <?php
                                                                    $cf7_sf_modules_fields = get_option( 'cf7_sf_modules_fields' );
                                                                    $cf7_sf_module_fields = ( isset( $cf7_sf_modules_fields[$cf7_sf_module] ) ? $cf7_sf_modules_fields[$cf7_sf_module] : array() );
                                                                    if ( ! is_array( $cf7_sf_module_fields ) ) {
                                                                        $cf7_sf_module_fields = array();
                                                                    }

                                                                    foreach ( $cf7_fields as $cf7_field_key => $cf7_field_value ) {
                                                                        ?>
                                                                            <tr>
                                                                                <td><?php echo $cf7_field_key; ?></td>
                                                                                <td>
                                                                                    <select name="cf7_sf_fields[<?php echo $cf7_field_key; ?>][key]">
                                                                                        <option value=""><?php _e( 'Select a field' ); ?></option>
                                                                                        <?php    
                                                                                            $type = '';
                                                                                            foreach ( $cf7_sf_module_fields as $key => $value ) {
                                                                                                $selected = '';
                                                                                                if ( isset( $cf7_sf_fields[$cf7_field_key][0] ) ) {
                                                                                                    $cf7_sf_fields[$cf7_field_key]['key'] = $cf7_sf_fields[$cf7_field_key][0];
                                                                                                }
                                                                                                if ( isset( $cf7_sf_fields[$cf7_field_key]['key'] ) && $cf7_sf_fields[$cf7_field_key]['key'] == $key ) {
                                                                                                    $selected = ' selected="selected"';
                                                                                                    $type = $value['type'];
                                                                                                }   
                                                                                                ?>
                                                                                                    <option value="<?php echo $key; ?>"<?php echo $selected; ?>>
                                                                                                        <?php echo $value['label']; ?> (<?php _e( 'Data Type:' ); ?> <?php echo $value['type']; echo ( $value['required'] ? __( ' and Field: required' ) : '' ); ?>)                                                                                                      
                                                                                                    </option>
                                                                                                <?php
                                                                                            }
                                                                                        ?>                                                                                   
                                                                                    </select>
                                                                                    <input type="hidden" name="cf7_sf_fields[<?php echo $cf7_field_key; ?>][type]" value="<?php echo $type; ?>" />
                                                                                </td>
                                                                            </tr>
                                                                        <?php
                                                                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    <?php
                                                } else {
                                                    ?><p><?php _e( 'No fields found.' ); ?></p><?php
                                                }                                         
                                            }
                                        }                                    
                                    ?>
                                    <p><input type='submit' class='button-primary' name="submit" value="<?php _e( 'Save' ); ?>" /></p>
                                </form>
                            <?php
                        } else {
                            ?>
                            <table class="widefat striped">
                                <thead>
                                    <tr>
                                        <th><?php _e( 'Title' ); ?></th>
                                        <th><?php _e( 'Status' ); ?></th>       
                                        <th><?php _e( 'Action' ); ?></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th><?php _e( 'Title' ); ?></th>
                                        <th><?php _e( 'Status' ); ?></th>       
                                        <th><?php _e( 'Action' ); ?></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php
                                        $args = array(
                                            'post_type'         => 'wpcf7_contact_form',
                                            'order'             => 'ASC',
                                            'posts_per_page'    => -1,
                                        );

                                        $forms = new WP_Query( $args );
                                        if ( $forms->have_posts() ) {                                
                                            while ( $forms->have_posts() ) {
                                                $forms->the_post();
                                                ?>
                                                    <tr>
                                                        <td><?php echo get_the_title(); ?></td>
                                                        <td><?php echo ( get_post_meta( get_the_ID(), 'cf7_sf', true ) ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>' ); ?></td>
                                                        <td><a href="<?php echo menu_page_url( 'cf7_sf-integration', 0 ); ?>&id=<?php echo get_the_ID(); ?>"><span class="dashicons dashicons-edit"></span></a></td>
                                                    </tr>
                                                <?php
                                            }                                
                                            wp_reset_postdata();
                                        } else {
                                            ?>
                                                <tr>
                                                    <td colspan="3"><?php _e( 'No forms found.' ); ?></td>
                                                </tr>
                                            <?php
                                        }                        
                                    ?>
                                </tbody>
                            </table>
                            <?php                         
                        } 
                    } else {
                        ?>
                            <div class="notice notice-error is-dismissible">
                                <p><?php _e( 'Please verify purchase code.' ); ?></p>
                            </div>
                        <?php
                    }
                ?>
            </div>
        <?php
    }
}