(function($){'use strict'
$.srSmoothscroll=function(options){var self=$.extend({step:55,speed:400,ease:'swing',target:$('body'),container:$(window)},options||{})
var container=self.container,top=0,step=self.step,viewport=container.height(),wheel=!1
var target
if(self.target.selector=='body'){target=(navigator.userAgent.indexOf('AppleWebKit')!==-1)?self.target:$('html')}else{target=container}
self.target.mousewheel(function(event,delta){wheel=!0
if(delta<0)
top=(top+viewport)>=self.target.outerHeight(!0)?top:top+=step
else top=top<=0?0:top-=step
target.stop().animate({scrollTop:top},self.speed,self.ease,function(){wheel=!1})
return!1})
container.on('resize',function(e){viewport=container.height()}).on('scroll',function(e){if(!wheel)
top=container.scrollTop()})}})(jQuery)