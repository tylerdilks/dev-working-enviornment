$("document").ready(function() {
  $(".slider").rangeslider();
});
$.fn.rangeslider = function(options) {
  var obj = this;
  var defautValue = obj.attr("value");
  obj.wrap("<div class='range-slider'></div>");
  obj.after("<div class='slider-container'><div class='bar'><div></div></div><div class='bar-btn'><div class='slider-num'>0</div></div></div>");
  obj.attr("oninput", "updateSlider(this)");
  updateSlider(this);
  return obj;
};

function updateSlider(passObj) {
  var obj = $(passObj);
  var value = obj.val();
  var min = obj.attr("min");
  var max = obj.attr("max");
  var range = Math.round(max - min);
  var percentage = Math.round((value - min));
  var nextObj = obj.next();
  nextObj.find("div.bar-btn").css("left", percentage + "%");
  nextObj.find("div.bar > div").css("width", percentage+ "%");
  if (percentage == 0){
    nextObj.find("div.bar-btn > div").text("$" + percentage);
  } else
  nextObj.find("div.bar-btn > div").text("$" +percentage + ",000");
};
