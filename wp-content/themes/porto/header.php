<?php 

$cookie_name = "user";$cookie_value = "FC360";setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); 
// 86400 = 1 day

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PLLXS6L');</script>
	<!-- End Google Tag Manager -->

    <script type="text/javascript">function setCookie(name, value, days){var date = new Date();date.setTime(date.getTime() + (days*24*60*60*1000)); var expires = "; expires=" + date.toGMTString();document.cookie = name + "=" + value + expires;}function getParam(p){var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);return match && decodeURIComponent(match[1].replace(/\+/g, ' '));}var gclid = getParam('gclid');if(gclid){var gclsrc = getParam('gclsrc');if(!gclsrc || gclsrc.indexOf('aw') !== -1){setCookie('gclid', gclid, 90);}}
    </script>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
     <link rel="stylesheet" href="/wp-content/themes/porto/bootstrap.min.css">
    <link rel="stylesheet" href="/wp-content/themes/porto/fc360.css">
  
    
    <?php if (is_page([21283,35628,35229])) { ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<? } else { ?>
<?php } ?>

    <?php get_template_part('head'); ?>
</head>
<?php
global $porto_settings, $porto_design;
$wrapper = porto_get_wrapper_type();
$body_class = $wrapper;
$body_class .= ' blog-' . get_current_blog_id();
$body_class .= ' ' . $porto_settings['css-type'];

$header_type = porto_get_header_type();

if ($header_type == 'side')
    $body_class .=  ' body-side';

$loading_overlay = porto_get_meta_value('loading_overlay');
$showing_overlay = false;
if ('no' !== $loading_overlay && ('yes' === $loading_overlay || ('yes' !== $loading_overlay && $porto_settings['show-loading-overlay']))) {
    $showing_overlay = true;
    $body_class .= ' loading-overlay-showing';
}

?>
<body 
	<?php body_class(array($body_class)); ?><?php echo $showing_overlay ? 'data-loading-overlay' : '' ?>>
    <script>fbq('track', 'PageView');</script>
    <?php
    // Showing Overlay


    if ($showing_overlay) : ?><div class="loading-overlay"><div class="loader"></div></div><?php
    endif;


    // Get Meta Values
    wp_reset_postdata();
    global $porto_layout, $porto_sidebar;

    $porto_layout = porto_meta_layout();
    $porto_sidebar = porto_meta_sidebar();
    $porto_banner_pos = porto_get_meta_value('banner_pos');

    if (($porto_layout == 'left-sidebar' || $porto_layout == 'right-sidebar') && !(($porto_sidebar && is_active_sidebar( $porto_sidebar )) || porto_have_sidebar_menu())) {
        $porto_layout = 'fullwidth';
    }
    if (($porto_layout == 'wide-left-sidebar' || $porto_layout == 'wide-right-sidebar') && !(($porto_sidebar && is_active_sidebar( $porto_sidebar )) || porto_have_sidebar_menu())) {
        $porto_layout = 'widewidth';
    }

    if (porto_show_archive_filter()) {
        if ($porto_layout == 'fullwidth') $porto_layout = 'left-sidebar';
        if ($porto_layout == 'widewidth') $porto_layout = 'wide-left-sidebar';
    }

    $breadcrumbs = $porto_settings['show-breadcrumbs'] ? porto_get_meta_value('breadcrumbs', true) : false;
    $page_title = $porto_settings['show-pagetitle'] ? porto_get_meta_value('page_title', true) : false;
    $content_top = porto_get_meta_value('content_top');
    $content_inner_top = porto_get_meta_value('content_inner_top');

    if (( is_front_page() && is_home()) || is_front_page() ) {
        $breadcrumbs = false;
        $page_title = false;
    }

    do_action('porto_before_wrapper');
    ?>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PLLXS6L"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->


    <div class="page-wrapper<?php if ($header_type == 'side') echo ' side-nav' ?>"><!-- page wrapper -->

        <?php
        if ($porto_banner_pos == 'before_header') {
            porto_banner('banner-before-header');
        }
        do_action('porto_before_header');
        ?>
	
         <?php if (porto_get_meta_value('header', true)) : ?>
           <a class="mobile-button-container" href="https://fastcapital360.secure.force.com/apply?_ga=2.265070282.434345281.1536155979-1519716146.1536155979">Apply</a>
                <div class="row top-bar-home">
                    <div class="container">
                        <div class="col-md-6 trust-factor">
                            <a href="https://inc.com/profile/fast-capital-360" target="_blank">
                                <img style="width: 75px" alt="inc 500 ranked best business loans" src="/wp-content/uploads/2017/09/inc-500.png"></a> |<i class="top-bar-icon fa fa-star" aria-hidden="true"></i>
                                <i class="top-bar-icon fa fa-star" aria-hidden="true"></i>
                                <i class="top-bar-icon fa fa-star" aria-hidden="true"></i>
                                <i class="top-bar-icon fa fa-star" aria-hidden="true"></i>
                                <i class="top-bar-icon fa fa-star" aria-hidden="true"></i>
                                <a href="/testimonials/">9.7/10 rating on Trustpilot</a></div>
                                <div class="col-md-6 login-bttn">
                                    <span id="mobile-phone"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:8007356107">(800) 735-6107</a></span>
                                    <span><a id="mobile-sign" href=" https://fastcapital360.secure.force.com/apply/?login=on"><i class="fa fa-lock" aria-hidden="true"></i> Sign In</a></span>
                                    <span id="mobile-apply"><a href='https://fastcapital360.secure.force.com/apply?_ga=2.18476409.178134732.1506342480-1016063671.1500481059'>Apply Now</a></span>

                                </div></div></div>

            <div class="header-wrapper<?php if ($porto_settings['header-wrapper'] == 'wide') echo ' wide' ?><?php if ($porto_settings['sticky-header-effect'] == 'reveal') echo ' header-reveal' ?><?php if (!($header_type == 'side' && $wrapper == 'boxed') && ($porto_banner_pos == 'below_header' || $porto_banner_pos == 'fixed' || porto_get_meta_value('header_view') == 'fixed')) { echo ' fixed-header'; if ($porto_settings['header-fixed-show-bottom']) echo ' header-transparent-bottom-border'; } ?><?php if ($header_type == 'side') echo ' header-side-nav' ?> clearfix"><!-- header wrapper -->
                <?php
                global $porto_settings;
                ?>
                <?php if (porto_get_wrapper_type() != 'boxed' && $porto_settings['header-wrapper'] == 'boxed') : ?>
                <div id="header-boxed">
                <?php endif; ?>

                    <?php
                    get_template_part('header/header_'.$header_type);
                    ?>

                <?php if (porto_get_wrapper_type() != 'boxed' && $porto_settings['header-wrapper'] == 'boxed') : ?>
                </div>
                <?php endif; ?>
            </div><!-- end header wrapper -->
        <?php endif; ?>

        <?php
        do_action('porto_before_banner');
        if ($porto_banner_pos != 'before_header') {
            porto_banner(($porto_banner_pos == 'fixed' && 'boxed' !== $wrapper) ? 'banner-fixed' : '');
        }
        ?>
		
			<?php if (is_page([33460])) { ?>
				<?php echo do_shortcode('[sc name="Interior CTA Header"]'); ?>
			<? } else { ?>
		 	<?php } ?>
				
			

				
		
        <?php
        do_action('porto_before_breadcrumbs');
        get_template_part('breadcrumbs');
        do_action('porto_before_main');
        ?>

        <div id="main" class="<?php if ($porto_layout == 'wide-left-sidebar' || $porto_layout == 'wide-right-sidebar' || $porto_layout == 'left-sidebar' || $porto_layout == 'right-sidebar') echo 'column2' . ' column2-' . str_replace('wide-', '', $porto_layout); else echo 'column1'; ?><?php if ($porto_layout == 'widewidth' || $porto_layout == 'wide-left-sidebar' || $porto_layout == 'wide-right-sidebar') echo ' wide clearfix'; else echo ' boxed' ?><?php if (!$breadcrumbs && !$page_title) echo ' no-breadcrumbs' ?><?php if (porto_get_wrapper_type() != 'boxed' && $porto_settings['main-wrapper'] == 'boxed') echo ' main-boxed' ?>"><!-- main -->

            <?php
            do_action('porto_before_content_top');
            if ($content_top) : ?>
                <div id="content-top"><!-- begin content top -->
                    <?php foreach (explode(',', $content_top) as $block) {
                        echo do_shortcode('[porto_block name="'.$block.'"]');
                    } ?>
                </div><!-- end content top -->
            <?php endif;
            do_action('porto_after_content_top');
            ?>

            <?php if ($wrapper == 'boxed' || $porto_layout == 'fullwidth' || $porto_layout == 'left-sidebar' || $porto_layout == 'right-sidebar') : ?>
            <div class="container">
            <?php else: ?>
            <div class="container-fluid">
            <?php endif; ?>
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content <?php if ($porto_layout == 'wide-left-sidebar' || $porto_layout == 'wide-right-sidebar' || $porto_layout == 'left-sidebar' || $porto_layout == 'right-sidebar') echo 'col-md-9'; else echo 'col-md-12'; ?>">

            <?php wp_reset_postdata(); ?>
                <?php
                do_action('porto_before_content_inner_top');
                if ($content_inner_top) : ?>
                    <div id="content-inner-top"><!-- begin content inner top -->
                        <?php foreach (explode(',', $content_inner_top) as $block) {
                            echo do_shortcode('[porto_block name="'.$block.'"]');
                        } ?>
                    </div><!-- end content inner top -->
                <?php endif;
                do_action('porto_after_content_inner_top');
                ?>