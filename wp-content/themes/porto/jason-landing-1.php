<?php
/*
Template Name: Jason Template 1
*/
?>

    
    <style>

    @media (max-width:485px) { 
        .inner-box { position: absolute; padding-top:50px!important; } 
        .col-sm-6.box { background-color:#fff!important; margin-top:0px!important; } 
        span.approval { font-size:20px!important; display:block; } 
        div#leadInfo { min-width: 320px!important; } 
        div#fundNeed { padding-left:0px!important; padding-right:0px!important;}
        .personal-btn { margin-left: 5px; margin-right: 5px; width: auto!important; min-width:auto!important; max-width:auto!important; } 
        #businessOwner, #leadInfo, #annualRevenue { padding-left: 0px!important; padding-right:0px!important; } 
        
        
        a.clear-button { display: inline-block!important; margin-top:0px!important; } 
        
        .row { margin-left: 25px!important; margin-right:25px!important; } 
        
        span.hero-call { font-size: 16px!important; font-weight:900; } 
        a.hero-phone { font-size: 18px!important; color: #78B625!important; } 
        div#business-time1 { margin:0!important; display:inline!important; float:inherit!important; } 
div#business-time2 { margin:0!important; display: inline!important; } 
.timeinbusiness-btn { width:100%!important; } 

.stepTitle { margin-bottom:0px!important; } 
        
        
        button.hero-button { 
  color: #26333C!important;
  font-family: Lato!important;
  font-size: 16px!important;
  font-weight: 900!important;
  letter-spacing: 0.6px!important;
  line-height: 32px!important;
  text-align: center;
  text-decoration:none!important;
}

.hero-button { height:49px; width:219px; border-radius:8px; background-color:#78B625; border:0px!important; margin-top: 0px; margin-bottom:15px;} 


        span.hero-call { display: block!important; } 
        
        section#main-section { padding-left: 25px!important; } 
        div#amountSeeking.container { padding-left:0px!important; padding-right:0px!important; } 
        
        .col-md-4.col-sm-6.col-xs-6.amount-btn { float:left!important; max-width:50%!important; width:50%!important; } 
        
        
        span.hero-mobile-title { font-family:Lato; font-weight: 500; font-size: 16px!important; line-height:24px!important; white-space:nowrap!important; } 
        
        
        .mobile-hero-title { font-size: 0px; } 
        span.hero-mobile-break { font-size: 28px!important; } 
        
        .col-xs-4 { width:60%!important; float:left!important; position:relative; display;block; text-align:left!important; margin-bottom:0px!important; margin-left:0px!important; } 
        
        section#main-section { min-height: 600px!important; } 
        
        
        
        
        
        .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto { width:auto!important; padding-left:0px!important; padding-right:0px!important; } 
        
        
        
        
        
        .row { margin-left:25px!important; margin-right:25px!important; } 
        
        b.medium.formula { padding-top:12px!important; padding-bottom:12px!important; } 
        
        .formula { width:270px!important; height:47px!important; display:block!important; margin-left:auto; margin-right:auto; } 
        
        .container-fluid.whitebg.before-arrow .col { min-width:100%!important; }
        .inner-blue-box b { white-space:normal!important; } 
       .loan-type-bottom-section a.clear-button { display:block!important; margin-left:-27%!important; margin-top:30%!important; } 
        
        .col-sm-6.box { margin-left: 0!important; width:100%!important; max-width:100%!important; } 
        
        section#whitearrow { display: none!important; } 
        .container-fluid.first.whitebg.sticky { display: none; } 
        b.medium { font-size:12px!important; } 
        img.img-padding { width: 270px!important; margin-top: 0px!important; display:block!important; float:inherit!important; height:205px!important; margin-left: auto!important; margin-right: auto!important; margin-bottom: -52px; margin-top: 30px!important; float:inherit!important; height: 207px; } 
        h2.heading { padding-top: 32px!important; } 
        .container-fluid.linearbg.after-arrow { padding-top: 50px!important; } 
        .col-sm-8.loan-bottom { margin-left:0px!important; } 
        img.loan-type-mini-icon { margin-left: 0px!important; } 
        .col-sm-4 { display:inline!important; } 
        .col-sm-8.loan-bottom { display: block; position: absolute; margin-left: 50px!important; } 
        .col-sm-8.loan-bottom b.medium { font-size: 18px!important; } 
        .col-sm-8.loan-boxed-bottom { display:inline!important; position: absolute;  margin-left: 0px!important; padding-left:45px!important; } 
        .container-fluid { padding-left: 0px!important; padding-right:0px!important; padding-bottom:32px!important;} 
        
        .inner-box { padding-top:50px!important; padding-left: 0px!important; padding-right: 0px!important; padding-bottom:32px!important; } 
        .col-sm-6.blue-box { margin-left:20px!important; margin-right:20px!important; } 
        
        img.image-margin { display:block!important; width:214px!important; height:185px!important; margin-left:auto; margin-right: auto; margin-top:-200px!important; position:relative!important; } 
        .col-sm-6.blue-box { margin-left:auto!important; margin-right:auto!important; } 
        
        .col-sm-6.blue-box.pull-right { margin-top: 40%!important; } 
        .inner-blue-box { padding-top: 42px!important; padding-left:25px!important; padding-right:25px!important; padding-bottom: 32px; } 
        b.blue-boxed-heading { width: 200px; } 
        b.medium.formula { padding-left:0px!important; text-align: center; } 
        
        
        
        .fixed-top { display: none; } 
    }
    
    
     div#columns { justify-content:center!important; } 
   
    section#main-section { background: linear-gradient(193.49deg, rgba(251, 251, 251, 0) 0%, #080808 100%),  url(http://67.225.129.230/~dev/loan-types/images/working-capital-hero.jpg); background-position: 90% 90%!important; background-size:cover!important; } 
    
      @media (max-width: 1439px) and (min-width:1107px) { 
  .col-sm-6.box { margin-left:auto!important; } 
    .col-sm-6.blue-box { margin-left:auto!important; } 
    .row { margin-left:100px!important; margin-right:100px!important; } 
    .row.nomargin { margin-left: 0px!important; margin-right:0px!important; } 
            
      }
      
    
    
   
    
    @media (max-width:1106px) and (min-width:486px) { 
        
        .hero-form { text-align:left; float:left; } 
        span.hero-call { display: inline!important; } 
        
        .col-xs-4 { margin-top:15px!important; text-align:left!important; float:left; display:block!important; margin-bottom:0px!important; } 
        
        
        span.hero-mobile-title { font-size: 24px!important; display:block; padding-top:56px; position:relative; text-align:left; } 
        
        span.hero-mobile-break { float: left; font-size:48px!important; line-height:64px!important; font-family:Lato; font-weighT:300; text-align:left; width:573px; margin-top:-60px; } 
        
        
     .col-sm.loan-type { min-width: 340px!important; max-width: 340px!important; width:340px!important; }    
     .row { margin-left:40px!important; margin-right: 40px!important; max-width:768px!important; margin:0 auto!important; } 
        svg.whitearrow { display: none!important; } 
        .col { padding-left:0px!important; padding-right:0px!important; min-width: 100%!important;} 
    img.img-padding { margin-top: 30px!important; margin: 0 auto; margin-bottom:-68px!important; display: block; float:none!important; }     
    .formula { margin: 0 auto; } 
    h2.heading.white { padding-top:0px!important; } 
    .col-sm-6.blue-box { margin-left: auto!important; max-width: 339px!important;}
    .row.nomargin { margin-left: 0px!important; } 
    .col-sm-6.box { margin-left: auto!important; max-width: 339px!important;} 
    .col-sm-3 { display: none; } 
   img.image-margin { width: 268px!important; height: 231px!important; top: -53%!important; left:10%!important; } 
    .inner-blue-box b { white-space:normal!important; } 
    b.blue-boxed-heading { white-space:nowrap!important; } 
    h2.heading { padding-top:48px!important; } 
    .col-sm-6.blue-box { margin-top:48px!important; padding-top:48px!important; } 
    .inner-blue-box { padding-top: 0px!important; padding-left: 32px!important; padding-right: 32px!important; padding-bottom: 35px!important; } 
    .inner-box { padding-top: 32px!important; padding-right: 32px!important; padding-left: 32px!important; padding-bottom: 32px!important; } 
    h2#qualifications { margin-top: 38px!important; } 
    .col-sm-6.box.pull-right { margin-top: 48px!important; } 
.col-sm-9 { min-width:100%!important; } 
a.tab-link { white-space:nowrap; } 
    }
    
      @media (min-width:1107px) { 
    
    .col { max-width: 50%!important; } 
      }
    
    
    
    
/*LOAN TYPE LANDING HERO CHANGE */
div#columns { max-width: 1399px!important; margin:0 auto!important; } 
.col-sm.loan-type { min-width: 342px!important; max-width: 342px!important;} 



section#main-section { min-height: 745px; margin: 0 auto; max-width:1440px; } 
.stepTitle { padding-top:0px!important; } 
svg#load { position: relative; } 
div#business-time1 { margin:0!important; display:inline; float: left; } 
div#business-time2 { margin:0!important; display: inline-block; } 

.col-md-4.col-sm-6.col-xs-6.amount-btn { max-width:33.3%; display:inline-block!important; } 
.hero-button { width: 126px!important; } 
.hero-form { text-align:left; } 
.small-title-desktop-new { font-size: 48px!important; line-height: 64px; font-family: Lato; font-weight: 300; max-width: 630px; height:192px; } 
.col-xs-4 { margin-bottom: -75px; margin-top: 50px!important;} 
div#amountSeeking.container { max-width:100%; } 

/* JASON CSS EDITS */
h1 { font-family: Lato; font-weight:300; color:#FFFFFF; font-size: 24px; line-height: 24px; text-align: left; } 
nav {
  white-space: nowrap;
  background: #37474F;
}
nav ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
/* Only stick if you can fit */
@media (min-height: 300px) {
  nav ul {
    position: sticky;
    top: 0;
  }
}
nav ul li a {
  display: block;
  padding: 0.5rem 1rem;
  color: white;
  text-decoration: none;
}
nav ul li a.current {
  background: black;
}


#loan-types {
    position: absolute;
    top: 800px;
}


#top-menu a:hover {
    color: #000;
}

#top-menu li.active a {
    border-top: 3px solid #333;
    color: #333;
}


#about {
    position: absolute;
    top: 400px;
}

html {
  scroll-behavior: smooth;
}
.col-sm-12.navigation { padding-left: 0px; padding-right: 0px; } 
ul#top-menu { list-style: none; padding-left:0px; text-align: center;} 
li { display: inline-block; width: 16.5%; } 
#top-menu { color: red; font-weight: 900; font-family: Lato; text-decoration:none; line-height:24px;  } 





/* HERO TABLET FIX */


@media (max-width: 991px) and (min-width: 800px) { 
    .wpb_single_image.wpb_content_element.vc_align_left.fc-360-logo { margin-left: 10%!important; } 
    
} 

@media (min-width: 992px) and (max-width: 1003px) { 
    .hero-mobile-title { padding-top: 71px!important; font-size: 29px!important; line-height: 32px; font-family: 'Lato' sans-serif; color:#fff; text-align:left; font-weight: 300; } 
     span.hero-mobile-break  { font-family: 'Lato' sans-serif; color:#fff; text-align: left; font-size: 16px; line-height: 24px; font-weight: 500; display: block; } 
    .mobile-heading { margin-top: 30px; } 
} 


@media (min-width: 1268px) { 
  .amount-btn { width:300px!important; margin-left:10px; margin-right:10px; }   
} 
@media (min-width: 993px) and (max-width: 1267px) { 
.vc_column_container.vc_col-sm-8.vc_custom_1538505580894.mpc-column { padding-left: 0px!important; } 
.menu { margin-right: 20%!important; } 
div#amountSeeking { margin-left: 15%!important; } 
.wpb_single_image.wpb_content_element.vc_align_left.fc-360-logo { margin-left: 20%!important; } 
div#testimonials { padding-left: 100px!important; padding-right:100px!important; } 
.footer-landing { padding-left: 100px!important; padding-right: 100px!important; } 
.hr-block { padding-left: 10%!important; padding-right: 10%!important; } 
.amount-btn { width: 280px!important; margin-left:10px; margin-right:10px; } 
 }


.btn-primary:hover, .button:hover, input.submit:hover, input[type="submit"].btn-primary:hover, input[type="submit"].button:hover, input[type="submit"].submit:hover, .btn-primary:active, .button:active, input.submit:active, input[type="submit"].btn-primary:active, input[type="submit"].button:active, input[type="submit"].submit:active, .btn-primary:focus, .button:focus, input.submit:focus, input[type="submit"].btn-primary:focus, input[type="submit"].button:focus, input[type="submit"].submit:focus {background-color: #8bd22d; }  

button.hero-button:visited, button.button:visited { background-color: #8bd22d; } 



@media (min-width: 485px) and (max-width: 1024px) {
    
    .timeinbusiness-btn { width: 100%; margin:0 auto!important; display: block; } 
    
}

@media (min-width: 992px) and (max-width: 1500px) 
{
#hero-background  {background: linear-gradient(193.49deg, rgba(251, 251, 251, 0) 0%, #080808 100%),  url(http://67.225.129.230/~dev/loan-types/images/working-capital-hero.jpg); background-position: 90% 90%!important; background-size:cover!important; } 
.col-md-6.form-wrapper { margin-left: -15px; } 
}

@media (min-width: 1501px)
{
#hero-background  {background: linear-gradient(193.49deg, rgba(251, 251, 251, 0) 0%, #080808 100%),  url(http://67.225.129.230/~dev/loan-types/images/working-capital-hero.jpg); background-position: 84% 65%!important; background-size:cover!important; } 

}

@media (min-width: 1px) and (max-width: 385px) 
{
    div#hero-background { background: linear-gradient(193.49deg, rgba(101, 101, 101, 0.5) 0%, #080808 100%),  url(http://67.225.129.230/~dev/loan-types/images/working-capital-hero.jpg); background-position: 65% 0px!important; background-size: cover!important; } 
    
}

@media (min-width: 1px) and (max-width: 385px) 
{
    div#hero-background { background: linear-gradient(193.49deg, rgba(101, 101, 101, 0.5) 0%, #080808 100%),  url(http://67.225.129.230/~dev/loan-types/images/working-capital-hero.jpg); background-position: 65% 0px!important; background-size: cover!important; } 
    
}

@media (min-width: 386px) and (max-width: 991px) 
{ 
    div#hero-background {background: linear-gradient(193.49deg, rgba(101, 101, 101, 0.5) 0%, #080808 100%),  url(http://67.225.129.230/~dev/loan-types/images/working-capital-hero.jpg); background-position: 59% 0%!important; background-size:cover!important; } 
    
}



@media (min-width: 486px) and (max-width:991px) {

button.hero-button:hover, button.button:hover { background-color: #8bd22d; }     
     .timeinbusiness-btn:hover{
    background:#8CC63F!important;
    cursor:pointer;
  }

  .personal-btn:hover{
    background:#8CC63F!important;
    cursor:pointer;
  }
  
    .hero-spacing { height: 32px!important; } 
    .wpb_single_image.wpb_content_element.vc_align_left.fc-360-logo { margin-left: 15px; } 
    .col-md-12 { padding-left:0px!important; padding-right:0px!important; } 
    .fa-navicon:before, .fa-reorder:before, .fa-bars:before { font-size: 20px; color: #fff; } 
    .wpb_single_image.wpb_content_element.vc_align_left.fc-360-logo { width: 200px; height: 19px; } 
    
    .stepTitle { padding-top: 0px!important; } 
    .col-xs-12 { text-align:center!important; } 
    img.trust-img-lp-2 { padding-left:5px!important; position:relative; margin: 0 auto!important; padding-top: 15px; } 
    
    .shiftnav .fa, .shiftnav-toggle .fa { font-family: FontAwesome!important; } 
    .fa { font-family:FontAwesome!important; } 
.shiftnav-toggle-button { background: none; }     
span.hero-call a { font-size: 18px; font-weight: bold; color:#78B625!important; } 
span.hero-call { display:block; margin-top:-30px!important; color:#ffffff!important; font-weight: bold; font-size: 16px; } 

.hero-button { height:49px; width:219px; border-radius:4px; background-color:#78B625; border:0px!important; margin-top: 40px; margin-bottom:40px;} 

button.hero-button { 
  color: #26333C!important;
  font-family: Lato!important;
  font-size: 16px!important;
  font-weight: 900!important;
  letter-spacing: 0.6px!important;
  line-height: 32px!important;
  text-align: center;
  text-decoration:none!important;
}
    

    .mobile-hero-title.mobile-heading { color:#fff!important; } 
    span.hero-mobile-title { font-size:48px; line-height:48px; color:#fff!important; font-weight:300!important;   } 
    span.hero-mobile-break { font-size:16px; line-height: 0px; } 
 
    
    
    .works-section p { padding-left: 25px; padding-right: 25px; } 
    p { color: #495863!important; font-size: 16px!important; line-height: 24px!important; } 
h3.loan-types-heading { font-size: 24px; color: #024B8B; font-weight: normal!important; margin-top: -20px; margin-bottom: -20px; } 
img.loan-type-svg { margin: 0 auto; display:block; } 
    * { outline: 0; }
    .button { height:49px; width:219px; border-radius:4px; background-color:#8CC63F; border:0px!important; } 
    button.button { 
  color: #26333C!important;
  font-family: Lato!important;
  font-size: 16px!important;
  font-weight: 900!important;
  letter-spacing: 0.6px!important;
  line-height: 32px!important;
  text-align: center;
  text-decoration:none!important;
}
}


@media (min-width: 992px) { 
    
    
    .business-owner-btn { padding-bottom:110px!important; } 
    
    .hr-block { padding-left: 250px; padding-right:250px; } 
    .copyright { color:#fff!important; font-size:14px!important; } 
    .center-row { padding-left:260px; padding-right: 260px; } 
.hide-desktop { display: none; } 




* {
    
    outline: 0; 
    
}



.main-container { min-width:120%; margin-top: 40px; } 


.yurr { background: linear-gradient(193.49deg, rgba(33, 35, 37, 0.51) 0%, #1b2227 100%),  url(http://67.225.129.230/~dev/loan-types/images/working-capital-hero.jpg)!important; background-position: 90% 90%!important; background-size:cover!important; } 


#businessOwner.sheet { display: none!important; } 
#businessOwner .sheet { display:none!important; } 
.sheet { display:none!important; } 

.make_me_blue{
  background-color: blue !important;
}

.col-xs-12 { text-align:center!important; }
img.trust-img-lp { padding-left: 12px; padding-right:12px;  }
img.trust-img-lp-1 { padding-left: 12px; padding-right:12px; padding-top:150px; }
img.trust-img-lp-3 { padding-left: 12px; padding-right:12px;  padding-top:194px;  }
img.trust-img-lp-2 { padding-left: 12px; padding-right:12px;  padding-top:10px;  }



button.hero-button:hover, button.button:hover { background-color: #8bd22d; } 
.col-xs-4 { padding:0px!important; } 


span.hero-call { font-weight: 900; color: #fff; margin-left: 16px; } 
a.hero-phone { font-weight: bold!important; color:#78B625!important; font-size:18px; padding-top:5px!important; } 

div#amountSeeking { margin-left:250px; } 

img.trust-img { padding-right: 24px; margin-bottom:120px; } 

span.button:hover { background-color: #8cc63fd4; } 

.submitBtn { max-width: 225px!important; height: 50px!important; } 

::-webkit-input-placeholder {
   font-style: italic;
}
:-moz-placeholder {
   font-style: italic;  
}
::-moz-placeholder {
   font-style: italic;  
}
:-ms-input-placeholder {  
   font-style: italic; 
}

.form-group { margin-bottom:9px!important; } 
.amount-row { margin: 0 auto; } 


div#business-time1 { padding-right:10px; } 
div#business-time2 { padding-left:10px; }
.timeinbusiness-btn:hover { border: 1px #8CC63F solid;  color: #495863;} 


.menu { float: right; margin-right: 250px; } 


h3.loan-types-heading { margin-bottom:11px!important; } 

.main-content.col-md-12 { background-color:#f5f8f8; } 
section.vc_section.vc_custom_1538664601821.vc_section-has-fill { max-width:940px!important; margin:0 auto!important; } 

.center-row { max-width:1500px!important; margin:0 auto!important; } 

section#main-section {  background:  linear-gradient(193.49deg, rgba(73,88,99,0) 0%, #495863 100%),  url(http://67.225.129.230/~dev/loan-types/images/working-capital-hero.jpg)!important;  background-position:60%85%!important;background-size:1727px 1140px!important;background-repeat:no-repeat!important; background-color:red!important; }  
.wpb_button, .wpb_content_element, ul.wpb_thumbnails-fluid>li { margin-bottom:0px!important; } 

.menu-item a:hover { 
 color:#26333C!important;
 padding-top:5px;
 padding-bottom:7px;
 border-radius:4px;
 text-decoration:none;
}


.menu-item {
    color: #fff!important;
    font-size: 16px;
    line-height: 24px;
    font-family: Lato!important;
    text-shadow: 0 1px 2px 0 rgba(0,0,0,0.21);
    padding-left: 13px!important;
    padding-right: 13px !important;
    white-space: nowrap;
    transition: all 0.1s ease-in;
}
.menu-item:hover{
    background: #8CC63F;
    padding: 10px;
    border-radius: 4px;
    color:#26333C!important;
}



.menu-item a { color:#fff!important; } 

.button { height:49px; width:219px; border-radius:4px; background-color:#8CC63F; border:0px!important; } 
.hero-button { height:49px; width:219px; border-radius:4px; background-color:#8CC63F; border:0px!important; margin-top: 40px; margin-bottom:40px;} 

button.hero-button { 
  color: #26333C!important;
  font-family: Lato!important;
  font-size: 16px!important;
  font-weight: 900!important;
  letter-spacing: 0.6px!important;
  line-height: 32px!important;
  text-align: center;
  text-decoration:none!important;
}


button.button { 
  color: #26333C!important;
  font-family: Lato!important;
  font-size: 16px!important;
  font-weight: 900!important;
  letter-spacing: 0.6px!important;
  line-height: 32px!important;
  text-align: center;
  text-decoration:none!important;
}
}





/* Bob */
@-webkit-keyframes hvr-bob {
  0% {
    -webkit-transform: translateY(-8px);
    transform: translateY(-8px);
  }
  50% {
    -webkit-transform: translateY(-4px);
    transform: translateY(-4px);
  }
  100% {
    -webkit-transform: translateY(-8px);
    transform: translateY(-8px);
  }
}
@keyframes hvr-bob {
  0% {
    -webkit-transform: translateY(-8px);
    transform: translateY(-8px);
  }
  50% {
    -webkit-transform: translateY(-4px);
    transform: translateY(-4px);
  }
  100% {
    -webkit-transform: translateY(-8px);
    transform: translateY(-8px);
  }
}
@-webkit-keyframes hvr-bob-float {
  100% {
    -webkit-transform: translateY(-8px);
    transform: translateY(-8px);
  }
}
@keyframes hvr-bob-float {
  100% {
    -webkit-transform: translateY(-8px);
    transform: translateY(-8px);
  }
}
.hvr-bob {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: perspective(1px) translateZ(0);
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
}
.hvr-bob:hover, .hvr-bob:focus, .hvr-bob:active {
  -webkit-animation-name: hvr-bob-float, hvr-bob;
  animation-name: hvr-bob-float, hvr-bob;
  -webkit-animation-duration: .3s, 1.5s;
  animation-duration: .3s, 1.5s;
  -webkit-animation-delay: 0s, .3s;
  animation-delay: 0s, .3s;
  -webkit-animation-timing-function: ease-out, ease-in-out;
  animation-timing-function: ease-out, ease-in-out;
  -webkit-animation-iteration-count: 1, infinite;
  animation-iteration-count: 1, infinite;
  -webkit-animation-fill-mode: forwards;
  animation-fill-mode: forwards;
  -webkit-animation-direction: normal, alternate;
  animation-direction: normal, alternate;
}




    /* BOOTSTRAP CSS START */





section#whitearrow { max-width: 1440px!important; margin: 0 auto;  } 
body { background-color:#f3f3f3!important; } 
    
.container-fluid.first.whitebg.sticky {
position: sticky; top: 0; z-index: 9999999; max-width: 1440px;
    
}
.col-sm-8.loan-bottom { color: #495863; } 


.container-fluid.whitebg.before-arrow { padding-bottom: 35px; max-width: 1440px; } 
.container-fluid.linearbg.after-arrow { padding-top: 100px; max-width: 1440px; } 
section#whitearrow { background: none; padding:0!important; } 

svg.whitearrow{
  position:absolute;
  bottom:-10px; left:0;
  width:100%; height:100px;
  display:block;
  fill: #ffffff;
  top: 0px;
}


svg{
  position:absolute;
  bottom:-10px; left:0;
  width:100%; height:100px;
  display:block;
  fill: #ffffff;
}


    b.medium.formula { padding-top: 20px; padding-left: 37px; display: block;} 
    .formula { width: 445px; height: 68px; background-color: #f5f8f8; } 

    img.img-padding { margin-top: 80px; float:right; }
    img.image-margin { position: absolute; top: -69%; } 
    img.medium-icon { display: block; margin: 0 auto; margin-bottom: 32px; } 
         /*TEXT */ 
   
        body { background-color:#ffffff; } 
        .row { margin-left: 167px; margin-right: 167px; max-width: 1440px;} 
        p { font-family:Lato; font-size: 16px; line-height:24px; color:#495863;  } 
        p.white { color:#ffffff!important; } 
        h2.heading.white { color:#ffffff; } 
        h2 { font-family:Lato; color:#024B8B; font-size: 32px; font-weight:bold; line-height:40px; } 
        h3 { font-family:Lato; font-size: 24px; line-height:32px; color:#024B8B; }
        h2.heading { padding-top:80px; } 
        
        /* LAYOUT */ 
        .container-fluid { padding-bottom: 80px; } 
        .container-fluid.whitebg { background-color:white; max-width: 1440px; } 
        .container-fluid.greybg { background-color: #f5f8f8; max-width:1440px; } 
        .container-fluid.first { padding-bottom: 0px; } 
        .container-fluid.linearbg {   background: linear-gradient(227.33deg, #398BD2 0%, #024B8B 100%); } 
        a.tab-link { text-decoration:none; font-weight: 900; font-family:Lato; color:#78B625; font-size: 16px; line-height: 24px; display:block; padding-bottom:18px; } 
        .col-sm-2.tab { border-bottom: 1px solid #D2D6D8; } 
        
        /* LOAN TYPES BOX */
        .loan-type-bottom-section { padding-top: 45px; } 
        b.medium { font-size: 18px; color: #024B8B; font-weight: bold; font-family: Lato; } 
        .col-sm-8.loan-bottom { margin-left: -55px; } 
        .col-sm-8.loan-boxed-bottom { margin-left: -65px; } 
        img.loan-type-mini-icon { margin-left: -25px; } 
        img.loan-type-icon { margin-top: 64px; margin-bottom: 16px; }
        hr { border-top: 2px solid #D2D6D8; } 
       h3.loan-type-heading { color: #78B625; } 
       .loan-type-inner { padding-left: 25px; padding-right: 25px; } 
        .col-sm.loan-type { background-color:#ffffff; margin: 4px; } 
        .col-sm.loan-type.nobg { background-color: #ff000000; } 
        
        .row.nomargin { margin-left: 0px; margin-right: 0px; } 
        
        
        /* BOX WITHIN LAYOUT */ 
        p.loan-type-content { max-height: 170px; padding-top:15px; margin-bottom:40px; } 
        .col-sm-6.box { background-color:#F5F8F8; max-width: 440px; max-height:370px; margin-top: 80px; margin-left: auto; } 
        
         .col-sm-6.blue-box { background-color:#E8F1F8; max-width: 440px; max-height:370px; margin-top: 80px; margin-left: auto; } 
               
        .inner-box { padding-top: 48px; padding-left: 48px; padding-right: 48px; padding-bottom: 48px; } 
        
         .inner-blue-box { padding-top: 76px; padding-left: 48px; padding-right: 48px; padding-bottom: 48px; } 
        
        .inner-box b, .inner-blue-box b{ color: #495863; font-size: 18px; line-height: 24px; font-family: Lato; } 
        
        .inner-blue-box b { white-space:nowrap; } 
        span.list { color:#78B625; font-weight: 900; font-family:Lato; font-size: 16px; line-height:24px; } 
        span.list a { color:#78B625;  }
          b.blue-boxed-heading { font-size: 16px; color: #024B8B; font-weight: 900; line-height: 24px; display:block;padding-top: 16px; font-family: Lato; white-space:nowrap;} 
          
                b.boxed-heading { white-space: nowrap; font-size: 16px; color: #78B625; font-weight: 900; line-height: 24px; display:block;padding-top: 16px; font-family: Lato;} 
                
                
          img.loan-type-boxed-icon { margin-left: -15px; margin-top: 16px; } 
          
          
          a.clear-button { text-decoration: none; font-weight: bold; color:#78B625; font-size: 16px; line-height:24px; font-family:Lato; border: 2px solid #8cc63f; padding-top: 12px; padding-bottom: 14px; border-radius: 8px; min-width: 140px; padding-left: 46px; padding-right: 46px; margin-left:-5px; margin-bottom:32px; margin-top:40px; } 
          
        
        
        /* HERO CTA CHANGES */
        
        div#amountSeeking.container { margin-left: 0px!important; } 
        section#main-section { background-color: black!important; background-size: cover!important; } 
        
        
        

        
    </style>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
          <link rel="stylesheet" type="text/css" href="https://www.fastcapital360.com/lp-resources/wp-landing-page.css">
        <link rel="stylesheet" type="text/css" href="http://67.225.129.230/~dev/loan-types/component.css">
    
    <script src="https://www.fastcapital360.com/lp-resources/wurfl.js"></script>
<script src="https://www.fastcapital360.com/lp-resources/validation-website.js"></script>
<script src="http://67.225.129.230/~dev/loan-types/js/resume.min.js"></script>

<script type="text/javascript">

function setCookie(name, value, days){
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000)); 
    var expires = "; expires=" + date.toGMTString();
    document.cookie = name + "=" + value + expires;
}
function getParam(p){
    var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
var gclid = getParam('gclid');
if(gclid){
    var gclsrc = getParam('gclsrc');
    if(!gclsrc || gclsrc.indexOf('aw') !== -1){
        setCookie('gclid', gclid, 90);
    }
}

  // Parse the URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var campaign = getParameterByName('utm_campaign');
var medium = getParameterByName('utm_medium');
var content = getParameterByName('utm_content');
var source = getParameterByName('utm_source');
var term = getParameterByName('utm_term');
 

    (function($, undefined) {

  "use strict";

  // When ready.
  $(function() {
    
    var $form = $( "#field-wrapper" );
    var $input = $form.find( "input" );

    $input.on( "keyup", function( event ) {
      
      
      // When user select text in the document, also abort.
      var selection = window.getSelection().toString();
      if ( selection !== '' ) {
        return;
      }
      
      // When the arrow keys are pressed, abort.
      if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
        return;
      }
      
      
      var $this = $( this );
      
      // Get the value.
      var input = $this.val();
      
      var input = input.replace(/[\D\s\._\-]+/g, "");
          input = input ? parseInt( input, 10 ) : 0;

          $this.val( function() {
            return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
          } );
    } );
  
  });
})(jQuery);
  </script>
  
  
      
      

<section id="main-section">
  <svg width="100%" height="100" viewBox="0 0 100 102" preserveAspectRatio="none">
    <path d="M0 0 L50 90 L100 0 V100 H0"/>
  </svg>
  <div id="amountSeeking" class="container">
      <div class="main-con" class="row">
        <div class="col-md-12 form-wrapper">
        <a href="https://www.fastcapital360.com/"><img src="http://67.225.129.230/~dev/wp-content/uploads/2018/09/fastcapital360-mobile@2x.png" width="220px" style="float:left; "></a>    
        
        <div class="hero-title desktop-heading"><h1>Working Capital Loans</h1></div>
        
        <div class="small-title-desktop-new desktop-heading">Aspirational headline about how you can succeed with a little boost.</div>
        
        <div class="mobile-hero-title mobile-heading"><span class="hero-mobile-title">Working Capital Loans</span><span class="hero-mobile-break"><br>Aspirational headline about how you can succeed with a little boost.</span></div>
    
              <div class="hero-form">
<button onclick="nextStep(),getTypedValue()" class="hero-button">Apply Now</button> <span class="hero-call">Or Call <a href="tel:8664993584" class="hero-phone">(866) 449-3584</a></span>
              </div>
              <div class="rates-text"></div>
                <div class="col-xs-4">
                  <img class="trust-img" src="https://www.fastcapital360.com/images/landing-1/google-stars.png"/>
      
                  <img class="trust-img" src="https://www.fastcapital360.com/images/landing-1/norton.png"/>
         
                  <img class="trust-img" src="https://www.fastcapital360.com/images/landing-1/inc-500.png"/>
             
                  <img class="trust-img" src="https://www.fastcapital360.com/images/landing-1/trust-pilot.png"/>
                </div>
        </div>
        <div class="col-md-6">
        </div>

      </div>

</div>

<div id="fundNeed" class="container hide w3-animate-left">
  <!-- <div class="title-underline"></div> -->
    <div class="stepTitle" style="font-weight:bold">How much funding do you need?</div>
      
      <div class="amount-row">
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="30000" id="amt-opt1" onclick="getValue1(); nextStep2()" class="personal-btn">
               $5,000 - $14,000
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="80000" id="amt-opt2" onclick="getValue2(); nextStep2()" class="personal-btn">
              $15,000 - $24,000
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="200000" id="amt-opt3" onclick="getValue3(); nextStep2()" class="personal-btn">
              $25,000 - $49,000
            </div>
        </div>
        

      
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="400000" id="amt-opt4" onclick="getValue4(); nextStep2()" class="personal-btn">
              $50,000 - $74,000
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="750000" id="amt-opt5" onclick="getValue5(); nextStep2()" class="personal-btn">
              $75,000 - $149,000
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="150000" id="amt-opt6" onclick="getValue6(); nextStep2()" class="personal-btn">
              $150,000 +
            </div>
        </div>
        

      </div>
<div class="col-xs-12">
                  <img class="trust-img-lp-3" src="https://www.fastcapital360.com/images/landing-1/google-stars.png"/>
      
                  <img class="trust-img-lp-3" src="https://www.fastcapital360.com/images/landing-1/norton.png"/>
         
                  <img class="trust-img-lp-3" src="https://www.fastcapital360.com/images/landing-1/inc-500.png"/>
             
                  <img class="trust-img-lp-3" src="https://www.fastcapital360.com/images/landing-1/trust-pilot.png"/>
                </div>
      </div>



<div id="annualRevenue" class="container hide w3-animate-left">
  <!-- <div class="title-underline"></div> -->
    <div class="stepTitle" style="font-weight:bold">What is your annual business revenue?</div>
      
      <div class="amount-row">
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="30000" id="annual-opt1" onclick="getAnnualValue1(); nextStep4()" class="personal-btn">
               $0 - $49,000
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="80000" id="annual-opt2" onclick="getAnnualValue2(); nextStep4()" class="personal-btn">
              $50,000 - $119,000
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="200000" id="annual-opt3" onclick="getAnnualValue3(); nextStep4()" class="personal-btn">
              $120,000 - $249,000
            </div>
        </div>
        

      
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="400000" id="annual-opt4" onclick="getAnnualValue4(); nextStep4()" class="personal-btn">
              $250,000 - $499,000
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="750000" id="annual-opt5" onclick="getAnnualValue5(); nextStep4()" class="personal-btn">
              $500,000 - $999,000
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 amount-btn">
            <div value="1000000" id="annual-opt6" onclick="getAnnualValue6(); nextStep4()" class="personal-btn">
              $1,000,000 +
            </div>
        </div>
        

      </div>
<div class="col-xs-12">
                  <img class="trust-img-lp-1" src="https://www.fastcapital360.com/images/landing-1/google-stars.png"/>
      
                  <img class="trust-img-lp-1" src="https://www.fastcapital360.com/images/landing-1/norton.png"/>
         
                  <img class="trust-img-lp-1" src="https://www.fastcapital360.com/images/landing-1/inc-500.png"/>
             
                  <img class="trust-img-lp-1" src="https://www.fastcapital360.com/images/landing-1/trust-pilot.png"/>
                </div>
      </div>

  <div id="businessOwner" class="container hide w3-animate-left">

        <!-- <div class="title-underline"></div> -->
        <div class="stepTitle" style="font-weight:bold">How long have you been in business?</div>
          
        <div style="padding-top:0px;" onclick="getBusinessValue1()" value="0" id="business-time1" class="col-md-6 col-sm-6 business-owner-btn">
            <div onclick="nextStep3()" style="margin-right:0px" class="timeinbusiness-btn">
              <img src="https://www.fastcapital360.com/lp-resources/img/startup.png" id="rocket" />
                <div class="btn-title">Just Started<br/><span style="font-size:15px; font-weight:normal;">(0-3 Months)</span></div>
            </div>
        </div>
        <div style="padding-top:0px;" onclick="getBusinessValue2()" value="1" id="business-time2"  class="col-md-6 col-sm-6 business-owner-btn">
            <div onclick="nextStep3()" style="margin-left:0px" class="timeinbusiness-btn">
              <img src="https://www.fastcapital360.com/lp-resources/img/shop.png" id="store-front" />
                <div class="btn-title">Established<br/><span style="font-size:15px; font-weight:normal;">(4 Months or more)</span></div>
            </div>
        </div> 


<div class="col-xs-12">
                  <img class="trust-img-lp" src="https://www.fastcapital360.com/images/landing-1/google-stars.png"/>
      
                  <img class="trust-img-lp" src="https://www.fastcapital360.com/images/landing-1/norton.png"/>
         
                  <img class="trust-img-lp" src="https://www.fastcapital360.com/images/landing-1/inc-500.png"/>
             
                  <img class="trust-img-lp" src="https://www.fastcapital360.com/images/landing-1/trust-pilot.png"/>
                </div>
  </div>

<div id="leadInfo" class="container hide w3-animate-top">
  <!-- <div class="title-underline"></div> -->
       <div class="stepTitle approval-style"><span class="approval">Great, you're <strong>pre-approved!</strong></span><br/><span style="font-size:16px">Create your account and continue to see loan options for free with no obligations.</span></div>
        <!-- <em><center style="color:#FFF;margin-bottom:25px">Estimated approval time: 4 minutes</center></em> -->
      
      <div class="row">
        <div class="col-md-12 col-md-offset-3">
          <div id="form-validation" class="form-validation alert alert-danger hide">Please complete the form below</div>
          <form>
            <div class="form-group">
              <input type="text" id="full-name" class="form-control leadInfo-field" placeholder="Full Name" required>
            </div>
            <div class="form-group">
              <input type="text" id="business-name" class="form-control leadInfo-field" placeholder="Business Name" required>
            </div>
            <div class="form-group">
              <input type="tel" onkeyup="phoneFormat()" id="business-phone" maxlength="10" class="form-control leadInfo-field" placeholder="Phone" required>
            </div>
            <div class="form-group">
              <input required type="text" id="owner-email" class="email-field form-control leadInfo-field" aria-describedby="emailHelp" onkeyup="emailFormatListener()" placeholder="Email">
            </div>
            
            <div onclick="getFormDetails()" class="btn submitBtn" id="this-is-the-goal">Continue</div>
           </form>
            <center style="color:#FFF;font-size:12px;margin-top:5px; padding-top:27px;"><i>Checking eligibility will not affect your credit score.</i><br> By clicking "Continue" you agree to our <a style="text-decoration:none;color:#78B625!important;" href="https://www.fastcapital360.com/legal-disclaimer/" target="_blank">Terms &amp; Conditions</a> and <a style="text-decoration:none;color:#78B625!important;" href="https://www.fastcapital360.com/privacy-policy/" target="_blank">Privacy Policy</a></center>
        </div>
      </div>
      <div class="col-xs-12">
                  <img class="trust-img-lp-2" src="https://www.fastcapital360.com/images/landing-1/google-stars.png"/>
      
                  <img class="trust-img-lp-2" src="https://www.fastcapital360.com/images/landing-1/norton.png"/>
         
                  <img class="trust-img-lp-2" src="https://www.fastcapital360.com/images/landing-1/inc-500.png"/>
             
                  <img class="trust-img-lp-2" src="https://www.fastcapital360.com/images/landing-1/trust-pilot.png"/>
                </div>
</div>


<div id="loadingScreen" class="container hide w3-animate-opacity">
   <div class="stepTitle" style="font-weight:bold">Finding the perfect program.</div>
    <div class="loading-bro">
  <svg id="load" x="0px" y="0px" viewBox="0 0 150 150">
    <circle id="loading-inner" cx="75" cy="75" r="60"/>
 </svg>
</div> 

</div>

</section>

   <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->

<div class="container-fluid whitebg">
 
  </div>
  
  
<div class="container-fluid first whitebg sticky">
  <div class="row">
      
      
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">
        <span class="d-block d-lg-none">Clarence Taylor</span>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/profile.jpg" alt="">
        </span>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#experience">Experience</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#education">Education</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#skills">Skills</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#interests">Interests</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#awards">Awards</a>
          </li>
        </ul>
      </div>
    </nav>
      
      
      
      
      
      
      
      
      
      <div class="col-sm-12 navigation">
        
        <nav>
        <ul>
          <li><a href="#about">About</a></li>
          <li><a href="#loan-types">Loan Types</a></li>
          <li><a href="#qualifications">Qualifications</a></li>
          <li><a href="#apply">How To Apply</a></li>
          <li><a href="#uses">Uses</a></li>
          <li><a href="#cost">Cost</a></li>
        </ul>
        </nav>
        
        </div>
      
      
  </div>
  
  <div class="row">
    
  <div class="col-sm-2 tab"><center><a href="#about" class="tab-link">About</a></center></div>
    <div class="col-sm-2 tab"><center><a href="#loan-types" class="tab-link">Loan Types</a></center></div>
      <div class="col-sm-2 tab"><center><a href="#qualifications" class="tab-link">Qualifications</a></center></div>
        <div class="col-sm-2 tab"><center><a href="#apply" class="tab-link">How to Apply</a></center></div>
          <div class="col-sm-2 tab"><center><a href="#uses" class="tab-link">Uses</a></center></div>
            <div class="col-sm-2 tab"><center><a href="#cost" class="tab-link">Cost</a></center></div>
  </div>    
  </div>
  

<div class="container-fluid whitebg before-arrow">
  <div class="row">
    <div class="col">
    <h2 class="heading"><a id="about"></a>What is a Working Capital Loan?</h2>
    <p>Working capital is defined as the difference between your current business assets and liabilities. Positive working capital is essential to your business’s financial success. If you are operating with insufficient capital, it can be difficult to move forward when you experience gaps in your cash flow.</p>

<p>A working capital loan can help your business manage these cash flow disruptions while still handling your routine expenses.</p>

    <div class="formula"><b class="medium formula">Business Assets - Liabilities = Working Capital</b></div>

<br><p>Positive working capital is essential to your business’s financial success. If you are operating with insufficient capital, it can be difficult to move forward when you experience gaps in your cash flow.</p>

<p>A working capital loan can help your business manage these cash flow disruptions while still handling your routine expenses.</p>

<p>Your business can use this type of loan for a number of reasons. It can be used to pay your employees and afford rent during your annual lulls or it can be used to purchase a vital piece of equipment without sacrificing the working capital you currently have.</p>

    </div>
    <div class="col">
      <img src="http://67.225.129.230/~dev/loan-types/images/illustration-working-capital-loan-woman-at-desk.svg" width="475px" height="362px" class="img-padding">
    </div>
  </div>
</div>

    <section id="whitearrow">
<svg width="100%" height="100" viewBox="0 0 100 102" preserveAspectRatio="none" class="whitearrow">
  <path d="M0 0 L50 100 L100 0 Z" />
</svg>
</section>

    <div class="container-fluid linearbg after-arrow">
          <div class="row">
  <div class="col-sm-9"><h2 class="heading white"><a id="loan-types"></a>How Do Working Capital Loans Work?</h2><p class="white">The way working capital loans work is straightforward, but it’s important to understand that the foundation of each program stems from a different loan type. If you are looking for funds to improve and maintain your operating capital, you should decide which option lines up with your particular needs.</p></div>
  <div class="col-sm-3"></div>

  </div>  
  
  <div id="columns" class="row">
    <div class="col-sm loan-type">
        <div class="loan-type-inner">
      <center><img src="http://67.225.129.230/~dev/loan-types/images/short-term-business-loan.svg" width="40px" height="40px" class="loan-type-icon"><h3 class="loan-type-heading">Short-Term Loan</h3></center>
      <p class="loan-type-content">A short-term loan provides your business with capital in a lump sum and is paid back in 18 months or less. Using this loan type to fund your working capital allows you to manage your cash flow gaps without committing to long-term payments.</p>
      
      <div class="loan-type-bottom-section">
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/calendar.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Time in Business<br><b class="medium">>1 Year</b></div>
      </div>
     
     <hr>
     
         <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/cash-register.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Annual Revenue<br><b class="medium">$180,000</b></div>
      </div>
      
      <hr>
      
          <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/gauge.svg" height="40px"  class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Credit Score<br><b class="medium">600</b></div>
      
       <a href="#" class="clear-button"  onclick="stepCheck();">Apply</a>
      </div>
      
     
      
      </div>
      
      </div>
    </div>
    <div class="col-sm loan-type">
         <div class="loan-type-inner">
      <center><img src="http://67.225.129.230/~dev/loan-types/images/merchant-cash-advance.svg" width="40px" height="40px" class="loan-type-icon"><h3 class="loan-type-heading">Merchant Cash Advance</h3></center>
      <p class="loan-type-content">A merchant cash advance provides capital in exchange for a percentage of your daily credit card sales until the advance is paid back in full, plus a set factor rate and fees. Using an MCA as your business working capital option gives you quick access to the cash you need when you’re pressed for time.</p>
        <div class="loan-type-bottom-section">
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/calendar.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Time in Business<br><b class="medium">>1 Year</b></div>
      </div>
     
     <hr>
     
         <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/cash-register.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Annual Revenue<br><b class="medium">$200,000</b></div>
      </div>
      
      <hr>
      
          <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/gauge.svg" height="40px"  class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Credit Score<br><b class="medium">550</b></div>
      
       <a href="#" class="clear-button"  onclick="stepCheck();">Apply</a>
      </div>
      
      </div>
      </div>
    </div>
    <div class="col-sm loan-type">
         <div class="loan-type-inner">
      <center><img src="http://67.225.129.230/~dev/loan-types/images/line-of-credit.svg" width="40px" height="40px" class="loan-type-icon"><h3 class="loan-type-heading">Line of Credit</h3></center>
      <p class="loan-type-content">A working capital line of credit is the option best for businesses who need flexible, renewable funding. With a line of credit, you are given a certain amount of funds and are able to pull from it as needed. Once you have replenished this balance, you can use the funds time and time again.</p>
           <div class="loan-type-bottom-section">
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/calendar.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Time in Business<br><b class="medium">>1 Year</b></div>
      </div>
     
     <hr>
     
         <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/cash-register.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Annual Revenue<br><b class="medium">$200,000</b></div>
      </div>
      
      <hr>
      
          <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/gauge.svg" height="40px"  class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Credit Score<br><b class="medium">640</b></div>
      
       <a href="#" class="clear-button"  onclick="stepCheck();">Apply</a>
      </div>
      
      </div>
      </div>
    </div>
 
  

    <div class="col-sm loan-type">
         <div class="loan-type-inner">
      <center><img src="http://67.225.129.230/~dev/loan-types/images/invoice-financing.svg" width="40px" height="40px" class="loan-type-icon"><h3 class="loan-type-heading">Invoice Financing</h3></center>
      <p class="loan-type-content">If your business is struggling with outstanding invoices and maintaining operational expenses, invoice financing could be your solution. Invoice financing provides your business with immediate access to the capital you’re waiting on to put towards essential operating costs.</p>
          <div class="loan-type-bottom-section">
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/calendar.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Time in Business<br><b class="medium">>1 Year</b></div>
      </div>
     
     <hr>
     
         <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/cash-register.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Annual Revenue<br><b class="medium">$150,000</b></div>
      </div>
      
      <hr>
      
          <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/gauge.svg" height="40px"  class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Credit Score<br><b class="medium">600</b></div>
      
       <a href="#" class="clear-button"  onclick="stepCheck();">Apply</a>
      </div>
      
      </div>
      </div>
    </div>
    <div class="col-sm loan-type">
         <div class="loan-type-inner">
      <center><img src="http://67.225.129.230/~dev/loan-types/images/sba-loan.svg" width="40px" height="40px" class="loan-type-icon"><h3 class="loan-type-heading">SBA Loan</h3></center>
      <p class="loan-type-content">SBA programs come with low-cost interest rates and terms comparable to a traditional bank loan. If you qualify for an SBA loan, your business can use the funds towards your working capital allowing you to take advantage of growth opportunities or recover from a financial loss.</p>
          <div class="loan-type-bottom-section">
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/calendar.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Time in Business<br><b class="medium">>4 Year</b></div>
      </div>
     
     <hr>
     
         <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/cash-register.svg" height="40px" class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Annual Revenue<br><b class="medium">$500,000</b></div>
      </div>
      
      <hr>
      
          <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/gauge.svg" height="40px"  class="loan-type-mini-icon"></div>
      <div class="col-sm-8 loan-bottom">Credit Score<br><b class="medium">680</b></div>
      
       <a href="#" class="clear-button"  onclick="stepCheck();">Apply</a>
      </div>
      
      </div>
      </div>
    </div>
    <div class="col-sm loan-type nobg">
      <h2 class="heading white" style="padding-top: 32px;" id="qualifications">Does My Business Qualify for a Working Capital Loan?</h2>
      <p class="white">Qualifying for a small business capital loan is not as hard as you may think. Newer businesses, even startups, can qualify for this kind of loan.</p>

<p class="white">While requirements are unique to each lender, the qualifications are dependent on the type of working capital loan you choose to take on.</p> 

<p class="white">If you would like to know if you qualify for a working capital loan, please contact one of our Business Advisors for more information. We’d be happy to provide some guidance towards the best program for your business’s needs.</p>
<h2 class="heading white" style="padding-top: 32px; margin-bottom: 32px;">(866) 449-3584</h2>
    </div>
   </div>
</div>


<div class="container-fluid greybg">
  <div class="row">
  <div class="col-sm-6"><h2 class="heading" id="apply">How Do I Apply for a Working
Capital Loan?</h2><p>Online business lenders offer quick and easy working capital loans. Applications are submitted online in minutes and only require basic contact and business information. Most lenders will ask you to provide recent bank statements in addition to your application. These documents will help your lender to determine a loan amount that fits your financial needs and structure.</p>
<p>While each lender wants to help your business get the working capital loan you need, every company’s process is different. Before diving into an application, be sure to do your research and find a lender that offers a wide variety of working capital programs. The more options you have available, the better your chance of finding a loan that’s right for you. 
</p>


<br><h3 style="color:#78B625; ">Do you need working capital? We can help you find the right kind of funding to get it fast!</h3>
  <br><a href="#" class="clear-button"  onclick="stepCheck();">Apply</a>


</div>


  <div class="col-sm-6 blue-box pull-right"><div class="inner-blue-box"><img src="http://67.225.129.230/~dev/loan-types/images/illustration-expert-advice.svg" width="348px" height="300px" class="image-margin"><b>Some other documents you may need are:</b>
  
    <div class="boxed-bottom-section">
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/identification.svg" height="24px" class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="blue-boxed-heading">Driver's License</b></div>
      </div>
     
         <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/receipt.svg" height="24px" class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="blue-boxed-heading">Business/Personal Tax Returns</b></div>
      </div>
      
          <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/sba-loan.svg" height="24px"  class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="blue-boxed-heading">Bank statements</b></div>
      </div>
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/check.svg" height="24px"  class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="blue-boxed-heading">Voided Business Check</b></div>
      </div>
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/proof-of-ownership.svg" height="32px"  class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="blue-boxed-heading">Proof of Ownership</b></div>
      </div>
      
      </div>
  
  
 </div></div>

  </div>    
  </div>
 

<div class="container-fluid greybg">
  <div class="row">
  <div class="col-sm-9"><h2 class="heading" id="uses">How Can My Business Use A Working Capital Loan?</h2><p>Considering working capital can be used in a number of ways, it's up to you to decide where the money should be spent. Here are some general examples of how you can use your future working capital loan: </p></div>
  <div class="col-sm-3"></div>

  </div>    
  </div>


   <div class="container-fluid greybg">
  <div class="row">
    <div class="col-sm">
         <img src="http://67.225.129.230/~dev/loan-types/images/figure-money-angel.svg"  width="210px" height="121px" class="medium-icon">
      <h3>Emergency Expenses</h3>
      <p>Emergencies arise when you least expect it. However, you should always be prepared for these situations.</p>

<p>Picture this - a vital piece of equipment breaks down and you’re forced to buy a brand new replacement. You can use your working capital loan to make that purchase and avoid dipping into the money you have set aside for other expenses.</p>

    </div>
    <div class="col-sm">
         <img src="http://67.225.129.230/~dev/loan-types/images/figure-chart-and-money.svg"  width="210px" height="121px" class="medium-icon">
      <h3>Growth Opportunities</h3>
      <p>If your business comes across an opportunity for growth, it could require more capital than you have available.</p>

<p>For instance, you may have a large purchase order from a new client and not enough inventory to fulfill it. A working capital loan can fund your expenses and be paid off quickly, once you turn that product around. With a working capital loan, you can keep money set aside for your daily expenses and take advantage of moments like these, all at once.</p>
    </div>
    <div class="col-sm">
        <img src="http://67.225.129.230/~dev/loan-types/images/figure-chart-and-coins.svg" width="210px" height="121px" class="medium-icon">
       <h3>Seasonal Peaks and Lows</h3>
      <p>Seasonal businesses can really benefit from a small business working capital loan.  Although busy seasons (like the holidays for retail stores) bring in a lot of revenue, businesses need a lot of capital to prepare beforehand.</p>

<p>On the other hand, some companies endure slow seasons (like a landscaping business during winter) and need help affording expenses like payroll. Whether you’re preparing for a yearly rush or dealing with a slow season, a working capital loan can provide the funds to get you through.</p>
    </div>
  </div>
</div>

<div class="container-fluid whitebg">
  <div class="row">
  <div class="col-sm-6"><h2 class="heading" id="cost">What Will a Working Capital Loan Cost?</h2>
  <p>Considering small business working capital loans derive from other loan types, the cost will depend on the program you choose.</p>
<p>If you already have a working capital loan in mind, one of our Business Advisors can help you get a better understanding of what each loan option may cost.</p>
<p>Each one of these programs is unique in its own way. Whether you’re trying to qualify, apply, or learn the cost of each program, your answers will vary.</p>
<p>It’s important to get all of the information you need before committing to a specific working capital loan program. Fast Capital 360 guides you through this process and gives you access to the working capital financing option that both fits your financial structure and needs.</p></div>
  <div class="col-sm-6 box pull-right"><div class="inner-box"><b>To learn more details about the cost of a working capital loan, you can refer to the links below:</b>
  
    <div class="boxed-bottom-section">
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/short-term-business-loan.svg" height="24px" class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="boxed-heading">Short-Term Loan</b></div>
      </div>
     
         <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/line-of-credit.svg" height="24px" class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="boxed-heading">Line of Credit</b></div>
      </div>
      
          <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/merchant-cash-advance.svg" height="24px"  class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="boxed-heading">Merchant Cash Advance</b></div>
      </div>
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/invoice-financing.svg" height="24px"  class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="boxed-heading">Invoice Financing</b></div>
      </div>
      
      <div class="row nomargin">
      <div class="col-sm-4"><img src="http://67.225.129.230/~dev/loan-types/images/sba-loan.svg" height="24px"  class="loan-type-boxed-icon"></div>
      <div class="col-sm-8 loan-boxed-bottom"><b class="boxed-heading">SBA Loan</b></div>
      </div>
      
      </div>
  
  
 </div></div>

  </div>    
  </div>





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 