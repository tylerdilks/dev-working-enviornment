<?php get_header(); ?>

<?php
global $porto_settings;
?>

<style>

.notfound-page{
    padding-top:40px;
}
.notfound-btn{
    background: #8CC63F;
    color: #fff;
    border-radius: 10px;
    text-transform: uppercase;
    padding: 20px;
    font-size: 1.5em;
    display: block;
    margin: 0 auto;
    margin-top: 30px;
}

.notfound-home{
    float:right;
    background:#203158;
}

.notfound-home:hover{
    background:#354d84;
}

.notfound-apply{
    float:left;
}

.notfound-apply:hover{
    background:#9bda47;
}

.notfound-title{
    font-size:20em;
    color:#203158;
    display:block;
    margin:0 auto;
    text-align:center;
    font-weight:bold;
}

.notfound-sub{
    font-size:4em;
    color:#203158;
    display:block;
    margin:0 auto;
    text-align:center;
    font-weight:bold;
    padding-top:110px
}

@media all and (max-width: 1007px) { 
.notfound-img{
    display:none;
}

.notfound-page{
    padding-bottom:50px;
 }

}

@media all and (max-width: 670px) {
.notfound-btn{
    font-size: 0.8em;
    padding: 11px;
}

.notfound-title{
    font-size:10em;
}

.notfound-sub{
    font-size:1.5em;
    padding-top:50px;
}

.notfound-page{
    padding-top:0px;
}

 }
</style>

<div id="content" class="no-content">
    <div class="container">
        <section class="page-not-found">
            <div class="row">
                <div class="col-md-12 notfound-page">
                <div class="col-md-6">
                    <div style="font-size:6em;color:#4c4c4c;font-weight:bold;margin-top:50px">Hmmm...</div>
                        <div style="font-size:2em;color:#4c4c4c;font-weight:bold;padding-top:50px;line-height:30px">We can't seem to find the page you're looking for.</div>
                            <div style="font-size:15px;color:#4c4c4c;margin-top:10px">Error Code: 404</div>
                                <div style="font-size:23px;color:#307fd7;;margin-top:10px;font-weight:bold;margin-bottom:10px">Here are some helpful links instead:</div>
                                    <div style="font-size:20px;color:#4c4c4c;margin-top:5px"><a href="/">Home</a></div>
                                    <div style="font-size:20px;color:#4c4c4c;margin-top:5px"><a href="/how-it-works/">How it Works</a></div>
                                    <div style="font-size:20px;color:#4c4c4c;margin-top:5px"><a href="/loan-types/">Loan Types</a></div>
                                    <div style="font-size:20px;color:#4c4c4c;margin-top:5px"><a href="/company/">Company</a></div>
                                    <div style="font-size:20px;color:#4c4c4c;margin-top:5px"><a href="/contact/">Contact</a></div>
                </div>
                <div class="col-md-6">
                    <img class="notfound-img" style="max-height:500px;height:100%;float:right" src="https://www.fastcapital360.com/wp-content/uploads/2018/09/404-1.png" />
                </div>                                               
                </div>
                <?php if ($porto_settings['error-block']) : ?>
                    <!-- <div class="col-md-4">
                        <?php //echo do_shortcode('[porto_block name="' . $porto_settings['error-block'] . '"]') ?>
                    </div> -->
                <?php endif; ?>
            </div>
        </section>
    </div>
</div>

<?php get_footer() ?>