<?php
/*
Template Name: Tyler Landing
*/
?>

<?php get_header() ?>
<style>
.page-top{
	display:none;
}
</style>
<link href="/wp-content/themes/porto/css/tyler-landing.css" rel="stylesheet">
<section id="hero">
        <div class="container">
            <div class="col-md-12">
                <div class="hero-title">Your Passion Is Your Business.<br/>Our Passion Is <strong style="font-weight:900">Growing</strong> Your Business.</div>
                  <div class="hero-sub-text">Pre-Qualify For Business Funding In As Quick As 30 Seconds.</div>
                    <div class="col-md-10 form-container">
                      <div class="row">
                        <div class="col-md-8 slider-container-wrapper">
                          <div class="slider-title">Annual Revenue</div>
                          <input class="slider" id="annual-revenue" value="20" min="0" max="100" name="rangeslider" type="range" />
                          <br/><br/>
                          <div class="slider-title">Amount Seeking</div>
                          <input class="slider" id="amount-seeking" value="20" min="0" max="100" name="rangeslider" type="range" />
                          <br/><br/>
                          <div class="slider-title" style="margin-bottom:10px;">Use of Funds</div>
                          <select id="use-of-funds" class="form-control">
                          <option value="">--None--</option>
                          <option value="Inventory">Inventory</option>
                          <option value="Expansion">Expansion</option>
                          <option value="Payroll">Payroll</option>
                          <option value="Receivables">Receivables</option>
                          <option value="Working Capital">Working Capital</option>
                          <option value="Contracts">Contracts</option>
                          <option value="Growth">Growth</option>
                          <option value="Marketing">Marketing</option>
                          <option value="Cash Flow">Cash Flow</option>
                          <option value="Consolidation">Consolidation</option>
                          <option value="Renovations">Renovations</option>
                          <option value="Equipment">Equipment</option>
                          <option value="Bridge">Bridge</option>
                          <option value="Taxes">Taxes</option>
                          <option value="Other">Other</option>
                        </select>
                        </div>
                        <div class="col-md-4 field-container">
                            <form>
                            <div class="form-group">
                              <input style="height: 40px; font-size: 16px;" type="text" class="form-control" id="user-name" placeholder="Name">
                            </div>
                            <div class="form-group">
                              <input style="height: 40px; font-size: 16px;" type="email" class="form-control" id="user-email" placeholder="Email">
                            </div>
                            <div class="form-group">
                              <input style="height: 40px; font-size: 16px;" type="tel" class="form-control" id="user-phone" placeholder="Phone">
                            </div>
                            <div class="form-group">
                              <input style="height: 40px; font-size: 16px;" type="text" class="form-control" id="business-name" placeholder="Business Name">
                            </div>
                            <button style="height: 40px; font-size: 16px;" onclick="myFunction();" class="btn submit-btn">Get Started</button>
                          </form>
                        </div>
                      </div>
                    </div>
            </div>
        </div>
    </section>
    <section id="how-it-works">
        <div class="container">
          <div class="row">
              <div class="col-md-6">
                  <div class="how-it-works-title">How Fast Capital 360 Funding Works.</div>
                  <br/>
                  <span class="step">1</span> Complete online application.<br/><br/>
                  <span class="step">2</span> Customize your funding program.<br/><br/>
                  <span class="step">3</span> Receive funds in your account.<br/><br/>
                  <span class="step">4</span> Use funds to grow your business.

              </div>
              <div class="col-md-6">
                 <center><img src="/wp-content/themes/porto/images/tyler-landing/mac.png" width="400" /></center>
              </div>
          </div>
        </div>
    </section>

    <section id="graphs">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="graph-cont">
                      <div class="bar-title">Average Approval Rate</div>
                      <div class="bar-graph bar1"><div class="graph-text">92.4%</div></div>
                        <hr style="background:#293f71"/>
                      <div class="bar-title">Repeat Customers</div>
                      <div class="bar-graph bar2"><div class="graph-text">72.3%</div></div>
                      <hr style="background:#293f71"/>
                      <div class="bar-title">Customer Satisfaction Rating</div>
                      <div class="bar-graph bar3"><div class="graph-text">98%</div></div>
                      <!-- <hr style="background:#293f71"/>
                      <div class="bar-title">Title</div>
                      <div class="bar-graph bar4"><div class="graph-text">20%</div></div> -->
                  </div>
                </div>
                <div class="col-md-6">
                  <center><iframe width="660px" height="370px" src="https://www.youtube.com/embed/H7VYXsqyG1Y?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></center>
                </div>
            </div>
        </div>
    </section>
    <section id="trust-pilot">
      <div class="trustpilot-widget" style="padding-top: 10px; padding-bottom: 10px; background-color: rgb(255, 255, 255) !important; position: relative;" data-locale="en-US" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="55ebc40a0000ff000582fc0d" data-style-height="130px" data-style-width="100%" data-theme="light" data-stars="4,5"><iframe frameborder="0" scrolling="no" title="Customer reviews powered by Trustpilot" src="https://widget.trustpilot.com/trustboxes/53aa8912dec7e10d38f59f36/index.html?locale=en-US&amp;templateId=53aa8912dec7e10d38f59f36&amp;businessunitId=55ebc40a0000ff000582fc0d&amp;styleHeight=130px&amp;styleWidth=100%25&amp;theme=light&amp;stars=4%2C5" style="position: relative; height: 130px; width: 100%; border-style: none; display: block; overflow: hidden;"></iframe></div>
    </section>
    <!-- <section>
    	<div class="container">
    		<div class="row">
    			<div class="col-md-6">
    				<center><img style="width:200px;height:180px;" src="https://www.forwardline.com/wp-content/uploads/2016/12/Complete-Application-Icon.svg" /></center>

    			</div>
    			<div class="col-md-6">
    				This is how it works text
    			</div>
    			<img style="margin-left:350px;" src="https://www.forwardline.com/wp-content/uploads/2016/12/HIW-Divider-1.png" />
    			<div class="col-md-6">
    				This is how it works text
    			</div>
    			<div class="col-md-6">
    				<center><img style="width:200px;height:180px;margin-right:70px" src="https://www.forwardline.com/wp-content/uploads/2016/12/Complete-Application-Icon.svg" /></center>
    			</div>
    			<center><img style="margin-right:40px;" src="https://www.forwardline.com/wp-content/uploads/2016/12/HIW-Divider-2.png" /></center>
    			<div class="col-md-6">
    				<center><img style="width:200px;height:180px;" src="https://www.forwardline.com/wp-content/uploads/2016/12/Complete-Application-Icon.svg" /></center>
    			</div>
    			<div class="col-md-6">
    				This is how it works text
    			</div>
    		</div>
    	</div>
    </section> -->
    <script>
      function myFunction(){
        var annual_revenue = document.getElementById("annual-revenue").value;
        var amount_seeking = document.getElementById("amount-seeking").value;
        var merchant_name = document.getElementById("user-name").value;
        var merchant_email = document.getElementById("user-email").value;
        var merchant_phone = document.getElementById("user-phone").value;
        var business_name = document.getElementById("business-name").value;
        var use_of_funds = document.getElementById("use-of-funds").value;
        var how_did_you_hear = "Search Engine"
        var website_link = 'https://fastcapital360.secure.force.com/apply/?amountSeeking=' + String(amount_seeking) + '&annualRevenue=' + String(annual_revenue) + '&useOfFunds=' + use_of_funds + '&businessName=' + business_name + '&siteleadownername=' + merchant_name + '&email=' + merchant_email + '&phone=' + String(merchant_phone) + '&HowDidYouHearAboutUs=' + how_did_you_hear;


        window.open(website_link);

      }

    </script>
        <!-- Bootstrap core JavaScript -->
    <script src="/wp-content/themes/porto/tyler-landing/vendor/jquery/jquery.min.js"></script>
    <script src="/wp-content/themes/porto/tyler-landing/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="/wp-content/themes/porto/js/range2.js"></script>

<?php get_footer() ?>