<?php
$cookie_name = "user";
$cookie_value = "FC360";
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://use.fontawesome.com/e19692520e.js"></script>
</head>
<style>
  legend{
    border-bottom:none;
  }
  .calculate-btn{
    background:#8CC63F;
    color:#FFF;
    font-size:20px;
    border:#1E73BE;
    border-radius:2px;
    display:block;
    margin:0 auto; 
    width:340px;
    height:50px;
  }
  .amount-field{
    border:none;
    width:250px;
    font-size: 50px;
    font-weight: 800;
    color: #000;
    text-align:center;
    background:#FFF;
    font-family: 'Open Sans', sans-serif;
  }
  .amount-field:before { 
    content:'$'; 
  }
  .no-pad{
    padding:0px;
  }
  .push-down{
    margin-top:150px;
    width:800px;
  }
  .title{
    font-weight:bold;
    font-size: 20px;
    text-transform: uppercase;
    text-align:center;
  }
  .modal-body{
    padding-top:0px;
  }
  .modal-header{
    border-bottom:none;
  }
  .modal-content{
    border:5px solid #1E73BE;
  }
  .marg-top{
    margin-top:20px;
  }
  .modal-title{
    text-align:center;
    font-weight:800;
    font-size:35px;
    line-height:30px;
    color:#1E73BE;
    margin-top:20px;
    font-family: 'Open Sans', sans-serif;
    padding-top:15px;
  }
  .results-title{
    text-align:center;
    font-weight:800;
    font-size:35px;
    line-height:40px;
    color:#1E73BE;
    margin-top:20px;
    font-family: 'Open Sans', sans-serif;
  }
  .disclaimer{
    text-align:center;
  }
  .disclaimer2{
    font-size:13px;
  }
  .hide{
    display:none;
  }
  .apply-btn{
    text-decoration:none;
  }
  .input-field{
    border-radius: 4px;
      border: 1px solid #ddd;
      font-size: 20px;
      padding: 10px;
      width:440px;
      display:block;
      margin:0 auto;
      height:50px;
      background:#DDD;
      color:#868686;
  }
  .revenue-input-field{
      border-bottom-right-radius: 4px;
      border-top-right-radius: 4px;
      border: 1px solid #ddd;
      font-size: 20px;
      padding: 10px;
      width:440px;
      display:block;
      height:50px;
      background:#DDD;
      color:#868686;
      outline:0;
  }
  .field-error-revenue{
    color:red;
  }
  .field-error-years{
    color:red;
  }
  .currency{
  display:none;
}
  .fa-arrows-h{
    font-size:40px;
  }
  ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    color: #adadad;
  }
  ::-moz-placeholder { /* Firefox 19+ */
    color: #adadad;
  }
  :-ms-input-placeholder { /* IE 10+ */
    color: #adadad;
  }
  :-moz-placeholder { /* Firefox 18- */
    color: #adadad;
}

.flex {
    display: flex;
    justify-content: flex-start;
    margin-left:43px;
}
.flex input {
    max-width: 400px;
    flex: 1 1 440px;
}
.right-radius{
  border-top-left-radius:4px;
  border-bottom-left-radius:4px;
}
.flex-big input{
    max-width: 440px;
    flex: 1 1 440px;
}
.flex .currency {
    font-size: 20px;
    padding: 0 10px 0 20px;
    color: #999;
    border-right: 0;
    line-height: 2.5;
    border-radius: 7px 0 0 7px;
    background: #ddd;
}
#demo{
  font-weight:bold;
  font-family: 'Open Sans' , sans-serif;
  color:red;
}
.loader {
  color: #1E73BE;
  font-size: 20px;
  margin: 100px auto;
  width: 1em;
  height: 1em;
  border-radius: 50%;
  position: relative;
  text-indent: -9999em;
  -webkit-animation: load4 1.3s infinite linear;
  animation: load4 1.3s infinite linear;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
}
@-webkit-keyframes load4 {
  0%,
  100% {
    box-shadow: 0 -3em 0 0.2em, 2em -2em 0 0em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 0;
  }
  12.5% {
    box-shadow: 0 -3em 0 0, 2em -2em 0 0.2em, 3em 0 0 0, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
  }
  25% {
    box-shadow: 0 -3em 0 -0.5em, 2em -2em 0 0, 3em 0 0 0.2em, 2em 2em 0 0, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
  }
  37.5% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 0, 2em 2em 0 0.2em, 0 3em 0 0em, -2em 2em 0 -1em, -3em 0em 0 -1em, -2em -2em 0 -1em;
  }
  50% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 0em, 0 3em 0 0.2em, -2em 2em 0 0, -3em 0em 0 -1em, -2em -2em 0 -1em;
  }
  62.5% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 0, -2em 2em 0 0.2em, -3em 0 0 0, -2em -2em 0 -1em;
  }
  75% {
    box-shadow: 0em -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0.2em, -2em -2em 0 0;
  }
  87.5% {
    box-shadow: 0em -3em 0 0, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0, -2em -2em 0 0.2em;
  }
}
@keyframes load4 {
  0%,
  100% {
    box-shadow: 0 -3em 0 0.2em, 2em -2em 0 0em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 0;
  }
  12.5% {
    box-shadow: 0 -3em 0 0, 2em -2em 0 0.2em, 3em 0 0 0, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
  }
  25% {
    box-shadow: 0 -3em 0 -0.5em, 2em -2em 0 0, 3em 0 0 0.2em, 2em 2em 0 0, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
  }
  37.5% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 0, 2em 2em 0 0.2em, 0 3em 0 0em, -2em 2em 0 -1em, -3em 0em 0 -1em, -2em -2em 0 -1em;
  }
  50% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 0em, 0 3em 0 0.2em, -2em 2em 0 0, -3em 0em 0 -1em, -2em -2em 0 -1em;
  }
  62.5% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 0, -2em 2em 0 0.2em, -3em 0 0 0, -2em -2em 0 -1em;
  }
  75% {
    box-shadow: 0em -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0.2em, -2em -2em 0 0;
  }
  87.5% {
    box-shadow: 0em -3em 0 0, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0, -2em -2em 0 0.2em;
  }
}

</style>
<body>
<?php
if(!isset($_COOKIE[$cookie_name])) { ?>

   <script type="text/javascript">
     setTimeout(showModal,15000);
     function showModal(){
  $('#myModal').modal('show');  
}
     </script> 

     <?php

     echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
     echo "Already Showed Modal Once";
}
?>
<div class="container">
<!--   <h2>You Could Qualify For</h2>
 
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Pop Over</button> -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="welcome-title">Need a quick estimate?<br/><span style="font-size:25px;">Results within seconds!</span></h4>
          <h4 class="results-title hide" id="welcome-title">Based on this information<br/>You may qualify for</h4>
        </div>
        <div class="modal-body">
         
  <div class="col-md-12 marg-top">
    <form name="estimate" id="qualify" action="">
    <fieldset>
      <p>
        <!-- <label for="revenue">Annual Revenue</label> -->
        <center>
        <div class="flex flex-big">
            <span class="currency">$</span>
        <input class="revenue-input-field revenue-input right-radius" id="revenue" name="revenue" placeholder="Annual Revenue" oninvalid="this.setCustomValidity('Please enter in Annual Revenue')" required/>
        </div>
        <div class="field-error-revenue hide">*Please enter in valid Annual Revenue</div>
      </p>
      <p>
        <!-- <label for="years">Years in Business</label> -->
        <input class="input-field" id="years" name="years" placeholder="Years in Business" oninvalid="this.setCustomValidity('Please enter in Years in Business')" required/></center>
        <center><p id="demo"></p></center>
      </p>
      <p>
        <input onclick="checkForm()" id="calc-btn" class="calculate-btn" type="submit" value="Get Estimate" data-toggle="collapse" data-target=".calculations" />
        
        <!-- <input type="reset" value="Reset" /> -->
      </p>
       <p id="text" class="disclaimer">FastCapital360 will not share your information with 3rd parties.<br/>Your security and privacy are our priority.</p>
  </div>
  <div class="col-md-12">
  <div class="loader hide"></div>
  <center>
  <div class="collapse">
       <p>
       <!--  <label class="title" for="qualifyAmount">You Qualify For Between:</label><br/> -->
        <input class="amount-field" id="qualifyAmount" name="qualifyAmount" />
        <label for="qualifyAmount2"><i class="fa fa-arrows-h" aria-hidden="true"></i></label>
        <input class="amount-field" id="qualifyAmount2" name="qualifyAmount2" />
          <div class="modal-title">Ready to get started?</div><br/>
            <input class="calculate-btn" type="submit" value="Apply Now" />
      </p>
      <center><p class="disclaimer2">This tool creates an estimate based on your annual revenue and time in business. This is not a guaranteed offer of funding. Whether your business would qualify for funding is subject to full underwriting and lender approval.Need more than the estimate?<br/>Call a Business Advisor at <a href="tel:8007356107">(800)-735-6107</a>.</p></center>
  </div>
  </center>
  </div>
  </fieldset>
    </form>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<script>
function showModal(){
  $('#myModal').modal('show');  
}

setInterval(checkforNan,100);

function checkforNan(){
  var x = document.forms["estimate"]["revenue"].value;
    if (x =="NaN") {
        $(".field-error-revenue").removeClass("hide");
        document.getElementById("revenue").value = "0";
        return false;
    }
    else if(x =="0"){
      $(".field-error-revenue").removeClass("hide");
    }
    else{
      $(".field-error-revenue").addClass("hide");
    }

}


function checkForm() {
    var x = document.forms["estimate"]["revenue"].value;
    var y = document.forms["estimate"]["years"].value;
    if (x == "") {
        // $(".field-error-revenue").removeClass("hide");
        return false;
    }
    else if(x =="NaN"){
      alert("This could work");
      return false;}
    else if(y == "") {
        // $(".field-error-years").removeClass("hide");
        return false;
    }
    else {
      // $(".field-error-revenue").addClass("hide");
      // $(".field-error-years").addClass("hide");
        validateForm();
    }
}

function loader(){
            $(".loader").addClass("hide");
            $(".collapse").removeClass("collapse");
          }

function validateForm() {
          setTimeout(loader,2000);
          
          var years, fundable;
          years = document.getElementById("years").value;
          fundable = (years < 1) ? "*Please Note: Most programs require a minimum of one year in business.":" ";
          document.getElementById("demo").innerHTML = fundable;
          $(".loader").removeClass("hide");
          $(".input-field").addClass("hide");
          $("#calc-btn").addClass("hide");
          $(".disclaimer").addClass("hide");
          $(".flex").addClass("hide");
          $("#welcome-title").addClass("hide");
          $(".modal-body").addClass("no-pad");
          $(".results-title").removeClass("hide");
          $("#qualifyAmount2").prop('disabled', true);
          $("#qualifyAmount").prop('disabled', true);
          var noCommas = $('.revenue-input').text().replace(/,/g, ''),
          asANumber = +noCommas;
}

$(document).ready(function(){
    $("input").focus(function(){
        $("span").css("display", "inline");
        $(".flex").removeClass("flex-big");
        $(".revenue-input-field").removeClass("right-radius");
    });
});

var fnf = document.getElementById("revenue");
fnf.addEventListener('keyup', function(evt){
    var n = parseInt(this.value.replace(/\D/g,''),10);
    fnf.value = n.toLocaleString();
}, false);




function addCommas(nStr){
 nStr += '';
 var x = nStr.split('.');
 var x1 = x[0];
 var x2 = x.length > 1 ? '.' + x[1] : '';
 var rgx = /(\d+)(\d{3})/;
 while (rgx.test(x1)) {
  x1 = x1.replace(rgx, '$1' + ',' + '$2');
 }
 return x1 + x2;
}

(function () {
  
  function calculateFunds(revenue, years) {
    revenue = parseFloat((revenue.replace(/,/g, '')));
    years = parseFloat(years);
    return ((revenue * 0.40)/12);
  }

  function calculateFunds2(revenue, years) {
    revenue = parseFloat((revenue.replace(/,/g, '')));
    years = parseFloat(years);
    return ((revenue * 0.80)/12);
  }

  var fundAmount = document.getElementById("qualify");
  if (fundAmount) {
    fundAmount.onsubmit = function () {
      this.qualifyAmount.value = ("$" + addCommas(-Math.round(-calculateFunds(this.revenue.value, this.years.value)/1000)*1000));
      this.qualifyAmount2.value = ("$" + addCommas(-Math.round(-calculateFunds2(this.revenue.value, this.years.value)/1000)*1000));
      return false;
    };
  }


}()); 
</script>
</body>

</html>