var fc360GAclientId;
var stepCounter = 0;


function getCookie(){
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&amp;]" + name + "=([^&amp;#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Give the URL parameters variable names
var source = getParameterByName('utm_source');
var medium = getParameterByName('utm_medium');
var campaign = getParameterByName('utm_campaign');
var term = getParameterByName('utm_term');

// Set the cookies
document.cookie="utmSource=" + source + ";expires=Wed, 18 Dec 2023 12:00:00 GMT" + ";domain=.fastcapital360.com;path=/"
document.cookie="utmMedium=" + medium + ";expires=Wed, 18 Dec 2023 12:00:00 GMT" + ";domain=.fastcapital360.com;path=/"
document.cookie="utmCampaign=" + campaign + ";expires=Wed, 18 Dec 2023 12:00:00 GMT" + ";domain=.fastcapital360.com;path=/"
document.cookie="utmTerm=" + term + ";expires=Wed, 18 Dec 2023 12:00:00 GMT" + ";domain=.fastcapital360.com;path=/"


alert("Cookies Set");

}

$("#menu-item-39966").click(function () {
 jQuery( '#shiftnav-main' ).shiftnav( 'closeShiftNav' );
});

$("#menu-item-39965").click(function () {
 jQuery( '#shiftnav-main' ).shiftnav( 'closeShiftNav' );
});



(function() {
window.__insp = window.__insp || [];
__insp.push(['wid', 436207787]);
var ldinsp = function(){
if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js?wid=436207787&r=' + Math.floor(new Date().getTime()/3600000); var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
setTimeout(ldinsp, 0);
})();




 function readCookie(name) { 
  name += '='; 
  for (var ca = document.cookie.split(/;\s*/), i = ca.length - 1; i >= 0; i--) 
  if (!ca[i].indexOf(name)) 
    return ca[i].replace(name, ''); 
} 

var gaUserCookie = readCookie("_ga"); 

if (gaUserCookie != undefined) { 
  var cookieValues = gaUserCookie.split('.');
  if (cookieValues.length > 2 ) 
  { 
    var userId = cookieValues[2]; 
    try {
      ga('set', 'dimension1', userId);
      ga('send', 'event', 'Custom Variables', 'Set UserId', {'nonInteraction': true});
    } catch(e) {}
   }  
}  
             

window.onload = function getGclid() {        
             fc360GAclientId = (name = new     RegExp('(?:^|;\\s*)gclid=([^;]*)').exec(document.cookie)) ? 
name.split(",")[1] : ""; }

  

 function nextStep(){

      var amountSeeking = document.getElementById("amountSeeking");
      amountSeeking.classList.add("hide");

      var fundNeed = document.getElementById("fundNeed");
      fundNeed.classList.remove("hide");
      
     var herobackground = document.getElementById("hero-background");
     herobackground.classList.add("yurr");

      window.scrollTo(0, 0);

      var step2Progress = document.getElementById("step2");
      step2Progress.classList.add("progress-step-complete");

      stepCounter = stepCounter + 1;
      dataLayer.push({'event': 'ppclpappstep1complete'});

        
  }



function stepCheck(){
   if (stepCounter > 0){
    window.scrollTo(0, 0);

   }else{
    nextStep();
   }
}

  function nextStep2(){
      stepCounter = 2;
      var fundNeed = document.getElementById("fundNeed");
      fundNeed.classList.add("hide");

      var businessOwner = document.getElementById("businessOwner");
      businessOwner.classList.remove("hide");

      window.scrollTo(0, 0);

      var step3Progress = document.getElementById("step3");
      step3Progress.classList.add("progress-step-complete");

 
      dataLayer.push({'event': 'ppclpappstep2complete'});
      

      

        
  }

  function nextStep3(){
     stepCounter = 3; 
    var businessOwner = document.getElementById("businessOwner");
      businessOwner.classList.add("hide");

      var annualRevenue = document.getElementById("annualRevenue");
      annualRevenue.classList.remove("hide");

      window.scrollTo(0, 0);

      var step4Progress = document.getElementById("step4");
      step4Progress.classList.add("progress-step-complete");

 

      dataLayer.push({'event': 'ppclpappstep3complete'});

  }


  function nextStep5(){
      stepCounter = 5;
    var loadingScreen = document.getElementById("loadingScreen");
      loadingScreen.classList.add("hide");

      var leadInfo = document.getElementById("leadInfo");
      leadInfo.classList.remove("hide");

      if (amountSeekingValue == 5000){
        document.getElementById("pre-qual-amount").innerHTML = "$14,000";
      } else if(amountSeekingValue == 20000){
        document.getElementById("pre-qual-amount").innerHTML = "$24,000";
      } else if(amountSeekingValue == 30000){
        document.getElementById("pre-qual-amount").innerHTML = "$49,000";
      } else if(amountSeekingValue == 60000){
        document.getElementById("pre-qual-amount").innerHTML = "$74,000";
      } else if(amountSeekingValue == 100000){
        document.getElementById("pre-qual-amount").innerHTML = "$149,000";
      }  else if(amountSeekingValue == 200000){
        document.getElementById("pre-qual-amount").innerHTML = "$250,000";
      } else{
        document.getElementById("pre-qual-amount").innerHTML = "$" + amountSeekingValue;
      }

      


      var progressBar4 = document.getElementById("progressBar4");
      progressBar.style.width = "100%";


  }



  function nextStep4(){
      stepCounter = 4;
      dataLayer.push({'event': 'ppclpappstep4complete'});
      var annualRevenue = document.getElementById("annualRevenue");
      annualRevenue.classList.add("hide");

      window.scrollTo(0, 0);

      var loadingScreen = document.getElementById("loadingScreen");
      loadingScreen.classList.remove("hide");

      setInterval(nextStep5, 2000);



  }

  

  var amountSeekingValue;
  var annualRevAmount;
  var timeInBusiness;
  var fullName;
  var businessName;
  var businessPhone;
  var ownerEmail;




  function getTypedValue(){
    amountSeekingValue = document.getElementById('amt-opt1').value;
  }

  function getValue1(){
    amountSeekingValue = document.getElementById('amt-opt1').getAttribute('value');
  }

  function getValue2(){
    amountSeekingValue = document.getElementById('amt-opt2').getAttribute('value');
  }

  function getValue3(){
    amountSeekingValue = document.getElementById('amt-opt3').getAttribute('value');
  }

  function getValue4(){
    amountSeekingValue = document.getElementById('amt-opt4').getAttribute('value');
  }

  function getValue5(){
    amountSeekingValue = document.getElementById('amt-opt5').getAttribute('value');
  }

  function getValue6(){
    amountSeekingValue = document.getElementById('amt-opt6').getAttribute('value');
  }

  function getAnnualValue1(){
    annualRevAmount = document.getElementById('annual-opt1').getAttribute('value');
  }

  function getAnnualValue2(){
    annualRevAmount = document.getElementById('annual-opt2').getAttribute('value');
  }

  function getAnnualValue3(){
    annualRevAmount = document.getElementById('annual-opt3').getAttribute('value');
  }

  function getAnnualValue4(){
    annualRevAmount = document.getElementById('annual-opt4').getAttribute('value');
  }

  function getAnnualValue5(){
    annualRevAmount = document.getElementById('annual-opt5').getAttribute('value');
  }

  function getAnnualValue6(){
    annualRevAmount = document.getElementById('annual-opt6').getAttribute('value');
  }

  function getBusinessValue1(){
    timeInBusiness = document.getElementById('business-time1').getAttribute('value');
  }

  function getBusinessValue2(){
    timeInBusiness = document.getElementById('business-time2').getAttribute('value');
  }

  function getFormDetails(){
    
    fullName = document.getElementById('full-name').value;
    businessName = document.getElementById('business-name').value;
    businessPhone = document.getElementById('business-phone').value;
    ownerEmail = document.getElementById('owner-email').value;

    if (fullName == ""){
      document.getElementById('full-name').style.border = "2px solid #f92d2d";
      var formValidation = document.getElementById("form-validation");
      document.getElementById("form-validation").innerHTML = "Please complete the Full Name field.";
      formValidation.classList.remove("hide");
    } else if(businessName == ""){
      document.getElementById('full-name').style.border = "1px solid #ccc";
      document.getElementById('business-name').style.border = "2px solid #f92d2d";
      var formValidation = document.getElementById("form-validation");
      document.getElementById("form-validation").innerHTML = "Please complete the Business Name field.";
      formValidation.classList.remove("hide");
    } else if(businessPhone == ""){
      document.getElementById('business-name').style.border = "1px solid #ccc";
      document.getElementById('business-phone').style.border = "2px solid #f92d2d";
      var formValidation = document.getElementById("form-validation");
      document.getElementById("form-validation").innerHTML = "Please complete the Phone field.";
        formValidation.classList.remove("hide");    
    } else if(ownerEmail == ""){
      document.getElementById('business-name').style.border = "1px solid #ccc";
      document.getElementById('business-phone').style.border = "1px solid #ccc";
      document.getElementById('owner-email').style.border = "2px solid #f92d2d";
      var formValidation = document.getElementById("form-validation");
      document.getElementById("form-validation").innerHTML = "Please complete the Email field.";
        formValidation.classList.remove("hide");

    } else if(ownerEmail.includes("@") != true){
      document.getElementById('business-name').style.border = "1px solid #ccc";
      document.getElementById('business-phone').style.border = "1px solid #ccc";
        document.getElementById('owner-email').style.border = "2px solid #f92d2d";
        var formValidation = document.getElementById("form-validation");
        document.getElementById("form-validation").innerHTML = "Please enter a valid Email address.";
        formValidation.classList.remove("hide");
    } else {
      document.getElementById('owner-email').style.border = "1px solid #ccc";
      document.getElementById('business-phone').style.border = "1px solid #ccc";
      var formValidation = document.getElementById("form-validation");
      formValidation.classList.add("hide");

      passToApplication();
    }

    
    
  }

  function passToApplication(){
    dataLayer.push({'event': 'appstep1complete'});
    window.location.href = "https://apply.fastcapital360.com/?amountSeeking=" + amountSeekingValue + "&annualRevenue=" + annualRevAmount + "&useOfFunds=Growth" + "&businessName=" + businessName + "&siteleadownername=" + fullName + "&email=" + ownerEmail + "&phone=" + businessPhone + "&HowDidYouHearAboutUs=Landing%20Page" + "&DeviceLP=" + device + "&gaUserID=" + fc360GAclientId  + "&utmCampaign=" + campaign + "&utmMedium=" + medium + "&utmSource=" + source + "&utmTerm=" + term + "&utmContent=" + content;



    
  }

  function phoneFormat(){
  var n = $('#business-phone').val();
  n = n.replace(/[^0-9]/g, '');
  n = n.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
  //return n;
  
  $('#business-phone').val(n);
}

function emailFormatListener(){
ownerEmail = document.getElementById('owner-email').value;
  if(ownerEmail.includes("@") == true){
      document.getElementById('owner-email').style.border = "1px solid #ccc";
      var formValidation = document.getElementById("form-validation");
      formValidation.classList.add("hide");
  }
}
  
var device;
$(".wurfl-mobile i").text(WURFL.is_mobile);
$(".wurfl-form-factor i").text(WURFL.form_factor);
$(".wurfl-device i").text(WURFL.complete_device_name);
if(WURFL.is_mobile){
  device = "Mobile";

}
else{
  device = "Desktop";

}
console.log(WURFL);