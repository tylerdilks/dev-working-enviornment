var fc360GAclientId;

 function readCookie(name) { 
  name += '='; 
  for (var ca = document.cookie.split(/;\s*/), i = ca.length - 1; i >= 0; i--) 
  if (!ca[i].indexOf(name)) 
    return ca[i].replace(name, ''); 
} 

var gaUserCookie = readCookie("_ga"); 

if (gaUserCookie != undefined) { 
  var cookieValues = gaUserCookie.split('.');
  if (cookieValues.length > 2 ) 
  { 
    var userId = cookieValues[2]; 
    try {
      ga('set', 'dimension1', userId);
      ga('send', 'event', 'Custom Variables', 'Set UserId', {'nonInteraction': true});
    } catch(e) {}
   }  
}  
             

window.onload = function getGclid() {        
             fc360GAclientId = (name = new     RegExp('(?:^|;\\s*)gclid=([^;]*)').exec(document.cookie)) ? 
name.split(",")[1] : ""; }

  

  function nextStep(){

      var amountSeeking = document.getElementById("amountSeeking");
      amountSeeking.classList.add("hide");

      var businessOwner = document.getElementById("businessOwner");
      businessOwner.classList.remove("hide");

      window.scrollTo(0, 0);

      var step2Progress = document.getElementById("step2");
      step2Progress.classList.add("progress-step-complete");

      dataLayer.push({'event': 'ppclpappstep1complete'});
  }

  function nextStep2(){


    var businessOwner = document.getElementById("businessOwner");
      businessOwner.classList.add("hide");

      var annualRevenue = document.getElementById("annualRevenue");
      annualRevenue.classList.remove("hide");

      window.scrollTo(0, 0);

      var step3Progress = document.getElementById("step3");
      step3Progress.classList.add("progress-step-complete");

      dataLayer.push({'event': 'ppclpappstep2complete'});

  }

  function nextStep4(){
    var loadingScreen = document.getElementById("loadingScreen");
      loadingScreen.classList.add("hide");

      var leadInfo = document.getElementById("leadInfo");
      leadInfo.classList.remove("hide");

      if (amountSeekingValue == 5000){
        document.getElementById("pre-qual-amount").innerHTML = "$14,000";
      } else if(amountSeekingValue == 20000){
        document.getElementById("pre-qual-amount").innerHTML = "$24,000";
      } else if(amountSeekingValue == 30000){
        document.getElementById("pre-qual-amount").innerHTML = "$49,000";
      } else if(amountSeekingValue == 60000){
        document.getElementById("pre-qual-amount").innerHTML = "$74,000";
      } else if(amountSeekingValue == 100000){
        document.getElementById("pre-qual-amount").innerHTML = "$149,000";
      }  else if(amountSeekingValue == 200000){
        document.getElementById("pre-qual-amount").innerHTML = "$250,000";
      } else{
        document.getElementById("pre-qual-amount").innerHTML = "$" + amountSeekingValue;
      }

      


      var progressBar4 = document.getElementById("progressBar4");
      progressBar.style.width = "100%";

      
  }

  function nextStep3(){
      dataLayer.push({'event': 'ppclpappstep3complete'});
      var annualRevenue = document.getElementById("annualRevenue");
      annualRevenue.classList.add("hide");

      window.scrollTo(0, 0);

      var loadingScreen = document.getElementById("loadingScreen");
      loadingScreen.classList.remove("hide");


      setInterval(nextStep4, 2000);

      

  }

  var amountSeekingValue;
  var annualRevAmount;
  var timeInBusiness;
  var fullName;
  var businessName;
  var businessPhone;
  var ownerEmail;

  function getTypedValue(){
    amountSeekingValue = document.getElementById('amt-opt1').value;
  }

  function getValue1(){
    amountSeekingValue = document.getElementById('amt-opt1').getAttribute('value');
  }

  function getValue2(){
    amountSeekingValue = document.getElementById('amt-opt2').getAttribute('value');
  }

  function getValue3(){
    amountSeekingValue = document.getElementById('amt-opt3').getAttribute('value');
  }

  function getValue4(){
    amountSeekingValue = document.getElementById('amt-opt4').getAttribute('value');
  }

  function getValue5(){
    amountSeekingValue = document.getElementById('amt-opt5').getAttribute('value');
  }

  function getValue6(){
    amountSeekingValue = document.getElementById('amt-opt6').getAttribute('value');
  }

  function getAnnualValue1(){
    annualRevAmount = document.getElementById('annual-opt1').getAttribute('value');
  }

  function getAnnualValue2(){
    annualRevAmount = document.getElementById('annual-opt2').getAttribute('value');
  }

  function getAnnualValue3(){
    annualRevAmount = document.getElementById('annual-opt3').getAttribute('value');
  }

  function getAnnualValue4(){
    annualRevAmount = document.getElementById('annual-opt4').getAttribute('value');
  }

  function getAnnualValue5(){
    annualRevAmount = document.getElementById('annual-opt5').getAttribute('value');
  }

  function getAnnualValue6(){
    annualRevAmount = document.getElementById('annual-opt6').getAttribute('value');
  }

  function getBusinessValue1(){
    timeInBusiness = document.getElementById('business-time1').getAttribute('value');
  }

  function getBusinessValue2(){
    timeInBusiness = document.getElementById('business-time2').getAttribute('value');
  }

  function getFormDetails(){
    
    fullName = document.getElementById('full-name').value;
    businessName = document.getElementById('business-name').value;
    businessPhone = document.getElementById('business-phone').value;
    ownerEmail = document.getElementById('owner-email').value;

    if (fullName == ""){
      document.getElementById('full-name').style.border = "2px solid #f92d2d";
      var formValidation = document.getElementById("form-validation");
      document.getElementById("form-validation").innerHTML = "Please complete the Full Name field.";
      formValidation.classList.remove("hide");
    } else if(businessName == ""){
      document.getElementById('full-name').style.border = "1px solid #ccc";
      document.getElementById('business-name').style.border = "2px solid #f92d2d";
      var formValidation = document.getElementById("form-validation");
      document.getElementById("form-validation").innerHTML = "Please complete the Business Name field.";
      formValidation.classList.remove("hide");
    } else if(businessPhone == ""){
      document.getElementById('business-name').style.border = "1px solid #ccc";
      document.getElementById('business-phone').style.border = "2px solid #f92d2d";
      var formValidation = document.getElementById("form-validation");
      document.getElementById("form-validation").innerHTML = "Please complete the Phone field.";
        formValidation.classList.remove("hide");    
    } else if(ownerEmail == ""){
      document.getElementById('business-name').style.border = "1px solid #ccc";
      document.getElementById('business-phone').style.border = "1px solid #ccc";
      document.getElementById('owner-email').style.border = "2px solid #f92d2d";
      var formValidation = document.getElementById("form-validation");
      document.getElementById("form-validation").innerHTML = "Please complete the Email field.";
        formValidation.classList.remove("hide");

    } else if(ownerEmail.includes("@") != true){
      document.getElementById('business-name').style.border = "1px solid #ccc";
      document.getElementById('business-phone').style.border = "1px solid #ccc";
        document.getElementById('owner-email').style.border = "2px solid #f92d2d";
        var formValidation = document.getElementById("form-validation");
        document.getElementById("form-validation").innerHTML = "Please enter a valid Email address.";
        formValidation.classList.remove("hide");
    } else {
      document.getElementById('owner-email').style.border = "1px solid #ccc";
      document.getElementById('business-phone').style.border = "1px solid #ccc";
      var formValidation = document.getElementById("form-validation");
      formValidation.classList.add("hide");

      passToApplication();
    }

    
    
  }

  function passToApplication(){
    dataLayer.push({'event': 'appstep1complete'});
    window.location.href = "https://apply.fastcapital360.com/?amountSeeking=" + amountSeekingValue + "&annualRevenue=" + annualRevAmount + "&useOfFunds=Growth" + "&businessName=" + businessName + "&siteleadownername=" + fullName + "&email=" + ownerEmail + "&phone=" + businessPhone + "&HowDidYouHearAboutUs=Landing%20Page" + "&DeviceLP=" + device + "&gaUserID=" + fc360GAclientId  + "&utmCampaign=" + campaign + "&utmMedium=" + medium + "&utmSource=" + source + "&utmTerm=" + term + "&utmContent=" + content;



    
  }

  function phoneFormat(){
  var n = $('#business-phone').val();
  n = n.replace(/[^0-9]/g, '');
  n = n.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
  //return n;
  
  $('#business-phone').val(n);
}

function emailFormatListener(){
ownerEmail = document.getElementById('owner-email').value;
  if(ownerEmail.includes("@") == true){
      document.getElementById('owner-email').style.border = "1px solid #ccc";
      var formValidation = document.getElementById("form-validation");
      formValidation.classList.add("hide");
  }
}
  
var device;
$(".wurfl-mobile i").text(WURFL.is_mobile);
$(".wurfl-form-factor i").text(WURFL.form_factor);
$(".wurfl-device i").text(WURFL.complete_device_name);
if(WURFL.is_mobile){
  device = "Mobile";

}
else{
  device = "Desktop";

}
console.log(WURFL);

